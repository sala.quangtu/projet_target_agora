﻿using UnityEngine;

[CreateAssetMenu(fileName = "RunnerInfo", menuName ="PlayerInfo/RunnerInfo", order = 150)]
public class SO_RunnerInfo : ScriptableObject
{
    public float moveSpeed;
    public float jumpHeight;
    public float gravity;
    public float reactivity;
    [Range(0,1)]
    public float accelTime;
    public float deccelTime;
    public float bounceForce;

    public void ActuMoveSpeed(string _newValue)
    {
        if(int.TryParse(_newValue, out int result))
        {
            moveSpeed = result;
        }
    }

    public void ActuJumpHeight(string _newValue)
    {
        if (int.TryParse(_newValue, out int result))
        {
            jumpHeight = result;
        }
    }

    public void ActuGravity(string _newValue)
    {
        if (int.TryParse(_newValue, out int result))
        {
            gravity = result;
        }
    }

    public void ActuReactivity(string _newValue)
    {
        if (int.TryParse(_newValue, out int result))
        {
            reactivity = result;
        }
    }

    public void ActuAccelTime(float _newValue)
    {
        accelTime = _newValue;
    }

    public void ActuDeccelTime(string _newValue)
    {
        if (int.TryParse(_newValue, out int result))
        {
            deccelTime = result;
        }
    }

    public void ActuBounceForce(string _newValue)
    {
        if (int.TryParse(_newValue, out int result))
        {
            bounceForce = result;
        }
    }
}