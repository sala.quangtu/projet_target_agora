﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingCursor : MonoBehaviour
{
    [SerializeField]
    SO_Cursor so_cursor = null;

    [SerializeField]
    GameObject HG = null;
    [SerializeField]
    GameObject BG = null;
    [SerializeField]
    GameObject HD = null;
    [SerializeField]
    GameObject BD = null;

    [Space, SerializeField]
    float m_dist = 0.5f;
    public void SetDist(float _value)
    {
        m_dist = _value;
    }

    [Space, SerializeField]
    float m_range = 1f;
    public void SetRange(float _value)
    {
        m_range = _value;
    }

    [Space, SerializeField]
    float m_maxRange = 2f;
    public void SetMaxRange(float _value)
    {
        m_maxRange = _value;
    }

    Vector3 m_maxDir;

    Vector3 m_initPosHG;
    Vector3 m_initPosBG;
    Vector3 m_initPosHD;
    Vector3 m_initPosBD;

    private void Start()
    {
        m_initPosHG = HG.transform.localPosition;
        m_initPosBG = BG.transform.localPosition;
        m_initPosHD = HD.transform.localPosition;
        m_initPosBD = BD.transform.localPosition;

        m_maxDir = -Vector3.forward * m_maxRange + Vector3.right * m_maxRange;
    }

    private void Update()
    {
        m_dist = so_cursor.Dist;
        m_range = so_cursor.Range;
        m_maxRange = so_cursor.MaxRange;

        m_maxDir = -Vector3.forward * m_maxRange + Vector3.right * m_maxRange;

        Vector3 _posHG = HG.transform.localPosition;
        Vector3 _posBG = BG.transform.localPosition;
        Vector3 _posHD = HD.transform.localPosition;
        Vector3 _posBD = BD.transform.localPosition;

        if(_posHG == m_initPosHG && 
           _posBG == m_initPosBG && 
           _posHD == m_initPosHD &&
           _posBD == m_initPosBD)
        {
            return;
        }

        _posHG = Vector3.MoveTowards(_posHG, m_initPosHG, m_dist);
        HG.transform.localPosition = _posHG;

        _posBG = Vector3.MoveTowards(_posBG, m_initPosBG, m_dist);
        BG.transform.localPosition = _posBG;

        _posHD = Vector3.MoveTowards(_posHD, m_initPosHD, m_dist);
        HD.transform.localPosition = _posHD;

        _posBD = Vector3.MoveTowards(_posBD, m_initPosBD, m_dist);
        BD.transform.localPosition = _posBD;
    }

    public void Shoot()
    {
        Vector3 _dir = -Vector3.forward * m_range + Vector3.right * m_range;

        if (Vector3.Distance(m_initPosHG, m_initPosHG + m_maxDir) <= Vector3.Distance(m_initPosHG, HG.transform.localPosition + _dir))
        {
            return;
        }

        HG.transform.Translate(_dir);
        BG.transform.Translate(_dir);
        HD.transform.Translate(_dir);
        BD.transform.Translate(_dir);
    }
}