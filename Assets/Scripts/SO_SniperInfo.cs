﻿using UnityEngine;

[CreateAssetMenu(fileName = "SniperInfo", menuName = "PlayerInfo/SniperInfo", order = 150)]
public class SO_SniperInfo : ScriptableObject
{
    public float moveSpeed;
    public float reactivity;
    public float cooldown;
    public float propulsionForce;

    public void ActuMoveSpeed(string _value)
    {
        if (float.TryParse(_value, out float result))
        {
            moveSpeed = result;
        }
    }

    public void ActuReactivity(string _value)
    {
        if (float.TryParse(_value, out float result))
        {
            reactivity = result;
        }
    }

    public void ActuCooldown(string _value)
    {
        if (float.TryParse(_value, out float result))
        {
            cooldown = result;
        }
    }

    public void ActuPropulsionForce(string _value)
    {
        if (float.TryParse(_value, out float result))
        {
            propulsionForce = result;
        }
    }
}