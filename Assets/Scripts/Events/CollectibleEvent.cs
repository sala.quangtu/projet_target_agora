﻿using UnityEngine.Events;

[System.Serializable]
public class CollectibleEvent : UnityEvent<CollectibleInfo>
{

}