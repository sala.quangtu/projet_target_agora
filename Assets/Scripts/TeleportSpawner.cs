﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportSpawner : MonoBehaviour
{
    public GameObject teleproterPrefab;

    public GameObject[] leftTele;
    public GameObject[] leftDoor;
    public GameObject[] rightTele;
    public GameObject[] rightDoor;
    
    public GameObject[] activeTele = new GameObject[2];
    public GameObject[] activeDoor = new GameObject[2];

    //GameObject activeTele1;
    //GameObject activeTele2;

    public int SpawnTime;
    float time = 0;

    float deltatTime = 0;
    public float teleportationDelay;

    public float timeBeforeDestroy;
    //public float timeD = 0;
    bool isTaken;
    public bool startCount;
    bool open;
    bool close;
    private void Start()
    {
        startCount = true;
        //for (int i = 0; i < rightTele.Length; i++)
        //{
        //    rightTele[i].SetActive(false);
        //}
        //for (int i = 0; i < leftTele.Length; i++)
        //{
        //    leftTele[i].SetActive(false);
        //}
    }

    private void Update()
    {
        deltatTime = Time.deltaTime;
        if (startCount) CountDown();
        //Debug.Log("close : " + close);
        if(open)
        {
            close = false;
            for (int i = 0; i < activeDoor.Length; i++)
            {
                for (int j = 0; j < activeDoor[i].transform.childCount; j++)
                {
                    if (activeDoor[i].transform.GetChild(j).name.Contains("L"))
                    {
                        activeDoor[i].transform.GetChild(j).transform.rotation = Quaternion.Lerp(activeDoor[i].transform.GetChild(j).transform.rotation, Quaternion.Euler(0, -60, 0), 2f * Time.deltaTime);
                    }
                    else if (activeDoor[i].transform.GetChild(j).name.Contains("R"))
                    {
                        activeDoor[i].transform.GetChild(j).transform.rotation = Quaternion.Lerp(activeDoor[i].transform.GetChild(j).transform.rotation, Quaternion.Euler(0, 60, 0), 2f * Time.deltaTime);
                    }
                   // activeDoor[i].transform.GetChild(j).transform.rotation = Quaternion.Lerp(activeDoor[i].transform.GetChild(j).transform.rotation, Quaternion.Euler(0, 60, 0), 2f * Time.deltaTime);
                    
                }
            }
            
        }
        if(close)
        {
                for (int i = 0; i < activeDoor.Length; i++)
                {
                    for (int j = 0; j < activeDoor[i].transform.childCount; j++)
                    {

                        if (activeDoor[i].transform.GetChild(j).name.Contains("L"))
                        {
                            activeDoor[i].transform.GetChild(j).transform.rotation = Quaternion.Lerp(activeDoor[i].transform.GetChild(j).transform.rotation, Quaternion.Euler(0, 180, 0), 2f * Time.deltaTime);
                        }
                        else if (activeDoor[i].transform.GetChild(j).name.Contains("R"))
                        {
                            activeDoor[i].transform.GetChild(j).transform.rotation = Quaternion.Lerp(activeDoor[i].transform.GetChild(j).transform.rotation, Quaternion.Euler(0, -180, 0), 2f * Time.deltaTime);
                        }

                        //var test = Quaternion.Lerp(activeDoor[activeDoor.Length].transform.GetChild(activeDoor[i].transform.childCount).transform.rotation, Quaternion.Euler(0, 180, 0), 2f * Time.deltaTime);
                        //if (test == Quaternion.Euler(0, 180, 0))
                        //{
                        //    close = false;   
                        //}
                    }
                }
        }
    }


    void CountDown()
    {
        if(time < SpawnTime)
        {
            time += deltatTime;
        }
        else
        {

            startCount = false;
            time = 0;
            SpawnTeleporter();
        }
    }
    
    void SpawnTeleporter()
    {
        var rand1 = UnityEngine.Random.Range(0, leftTele.Length);
        var rand2 = UnityEngine.Random.Range(0, rightTele.Length);

        //GameObject go1 = Instantiate(teleproterPrefab) as GameObject;
        //go1.transform.position = leftTele[rand1].transform.position + Vector3.up + Vector3.forward;
        //go1.GetComponent<TeleportDetection>().SetTs(this);
        //GameObject go2 = Instantiate(teleproterPrefab) as GameObject;
        //go2.transform.position = rightTele[rand2].transform.position + Vector3.up + Vector3.forward;
        //go2.GetComponent<TeleportDetection>().SetTs(this);
        
        
        activeTele[0] = leftTele[rand1];
        activeDoor[0] = leftDoor[rand1];
        activeTele[1] = rightTele[rand2];
        activeDoor[1] = rightDoor[rand2];
        for (int i = 0; i < activeTele.Length; i++)
        {
            activeTele[i].SetActive(true);
        }
        open = true;
    }

    public void ResetTeleporter()
    {
        for (int i = 0; i < activeTele.Length; i++)
        {
            activeTele[i].SetActive(false);
        }
        Array.Clear(activeTele, 0, activeTele.Length);
        startCount = true;

    }

    public Vector3 GetTargetTeleporter(GameObject go)
    {
        Vector3 v = Vector3.zero ;
        for (int i = 0; i < activeTele.Length; i++)
        {
            if(activeTele[i] != go)
            {
                v = activeTele[i].transform.position;
            }
        }
        open = false;
        close = true;
        return v;
    }

}
