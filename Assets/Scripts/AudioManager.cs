﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    private AudioSource m_audioSource = null;
    [SerializeField]
    private AudioClip m_mus_menu_intro = null;
    [SerializeField]
    private AudioClip m_mus_menu_lp = null;
    [SerializeField]
    private AudioClip m_mus_glace = null;
    [SerializeField]
    private AudioClip m_mus_automne = null;
    [SerializeField]
    private AudioClip m_mus_neon = null;
    [SerializeField]
    private AudioClip m_mus_spring = null;

    private void Awake()
    {
        m_audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Menu2")
            || SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Fin"))
        {
            if (m_audioSource.clip == m_mus_menu_intro
                && m_mus_menu_intro.length - 0.2f <= m_audioSource.time)
            {
                m_audioSource.clip = m_mus_menu_lp;
                m_audioSource.loop = true;
                m_audioSource.Play();
            }
        }
    }

    public void DropTheVolume(float _waitForSecond)
    {
        if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Fin"))
        {
            return;
        }

        StartCoroutine(DropVolume(_waitForSecond));
    }

    public void NewScene()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Ice"))
        {
            StopAllCoroutines();
            m_audioSource.volume = 1;
            m_audioSource.time = 0;
            m_audioSource.loop = false;
            m_audioSource.clip = m_mus_glace;
            m_audioSource.Play();
        }
        else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Automne"))
        {
            StopAllCoroutines();
            m_audioSource.volume = 1;
            m_audioSource.time = 0;
            m_audioSource.loop = false;
            m_audioSource.clip = m_mus_automne;
            m_audioSource.Play();
        }
        else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Neon"))
        {
            StopAllCoroutines();
            m_audioSource.volume = 1;
            m_audioSource.time = 0;
            m_audioSource.loop = false;
            m_audioSource.clip = m_mus_neon;
            m_audioSource.Play();
        }
        else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Vegetation"))
        {
            StopAllCoroutines();
            m_audioSource.volume = 1;
            m_audioSource.time = 0;
            m_audioSource.loop = false;
            m_audioSource.clip = m_mus_spring;
            m_audioSource.Play();
        }
        else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Fin"))
        {
            StopAllCoroutines();
            m_audioSource.volume = 1;
            m_audioSource.time = 0;
            m_audioSource.loop = false;
            m_audioSource.clip = m_mus_menu_intro;
            m_audioSource.Play();
        }
        else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Transition"))
        {
            StopAllCoroutines();
            m_audioSource.volume = 1;
            m_audioSource.time = 0;
            m_audioSource.loop = false;
            m_audioSource.clip = m_mus_menu_intro;
            m_audioSource.Play();
        }
        //else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Menu2"))
        //{
        //    StopAllCoroutines();
        //    m_audioSource.volume = 1;
        //    m_audioSource.time = 0;
        //    m_audioSource.loop = false;
        //    m_audioSource.clip = m_mus_menu_intro;
        //    m_audioSource.Play();
        //}
    }

    private IEnumerator DropVolume(float _waitForSecond)
    {
        while(_waitForSecond >= 0 || m_audioSource.volume >= 0)
        {
            _waitForSecond -= Time.deltaTime;
            m_audioSource.volume = _waitForSecond/FindObjectOfType<Transition>().TransitionTime;
            yield return null;
        }
    }
}