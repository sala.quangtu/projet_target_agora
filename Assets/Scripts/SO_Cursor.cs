﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Cursor", menuName = "SniperSettings/Cursor", order = 150)]
public class SO_Cursor : ScriptableObject
{
    public float Dist;
    public float Range;
    public float MaxRange;

    public void ActuDist(string _newValue)
    {
        float result = 0.05f;
        if (float.TryParse(_newValue, out result))
        {
            Dist = result;
        }
    }

    public void ActuRange(string _newValue)
    {
        float result = 0.5f;
        if (float.TryParse(_newValue, out result))
        {
            Range = result;
        }
    }

    public void ActuMaxRange(string _newValue)
    {
        float result = 0.75f;
        if (float.TryParse(_newValue, out result))
        {
            MaxRange = result;
        }
    }
}
