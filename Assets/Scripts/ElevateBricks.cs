﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevateBricks : MonoBehaviour
{
    public float radius = 5;
    public float height = 1;
    public float speed = 1f;

    public LayerMask brickMask = ~0;

    public AnimationCurve fallOff = new AnimationCurve();
    public AnimationCurve radiusFallOff = new AnimationCurve();
    Renderer[] renderers;
    private void Start()
    {
        renderers = transform.GetComponentsInChildren<Renderer>();
        MaterialPropertyBlock prop = new MaterialPropertyBlock();
        renderers[0].GetPropertyBlock(prop);
        prop.SetFloat(Shader.PropertyToID("shaderShfc_maxDist"), height);
        foreach (Renderer rd in renderers)
        {
            rd.SetPropertyBlock(prop);
        }
        
    }
    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    AddWave(transform.position);
        //}

        CalculateWave(Time.deltaTime);
    }

    List<Tuple<float, Vector3, Collider[]>> waves = new List<Tuple<float, Vector3, Collider[]>>();

    public void AddWave(Vector3 spawnPosition)
    {
        spawnPosition.z = transform.position.z;
        float distance = 0;
        waves.Add(new Tuple<float, Vector3, Collider[]>(distance, spawnPosition, Physics.OverlapSphere(spawnPosition, radius, brickMask)));
    }

    public void CalculateWave(float deltaTime)
    {
        float currentBrickDistance = 0f;
        float currentRatioDistance = 0f;
        for (int i = 0; i < waves.Count; i++)
        {
            if (waves[i].GetT() < radius + 2)
            {
                currentRatioDistance = radiusFallOff.Evaluate(waves[i].GetT() / radius);
                for (int j = 0; j < waves[i].GetV().Length; j++)
                {
                    currentBrickDistance = Mathf.Abs((waves[i].GetU() - waves[i].GetV()[j].transform.position).magnitude - waves[i].GetT());
                    if (i == 0)
                    {
                        waves[i].GetV()[j].transform.localPosition =
                            new Vector3(waves[i].GetV()[j].transform.localPosition.x,  waves[i].GetV()[j].transform.localPosition.y, fallOff.Evaluate(currentBrickDistance) * height * currentRatioDistance);

                    }
                    else
                    {
                        waves[i].GetV()[j].transform.localPosition =
                            new Vector3(waves[i].GetV()[j].transform.localPosition.x,  waves[i].GetV()[j].transform.localPosition.y, waves[i].GetV()[j].transform.localPosition.z + fallOff.Evaluate(currentBrickDistance) * height * currentRatioDistance);
                    }
                }
                waves[i].setT(waves[i].GetT() + Time.deltaTime * speed);
            }
            else
            {
                waves.RemoveAt(i);
            }
        }
    }
}

public class Tuple<T, U, V>
{
    T varOne;
    U varTwo;
    V var3;

    public Tuple(T t, U u, V v)
    {
        varOne = t;
        varTwo = u;
        var3 = v;
    }

    public void setT(T value)
    {
        varOne = value;
    }

    public void setU(U value)
    {
        varTwo = value;
    }

    public void setV(V value)
    {
        var3 = value;
    }

    public T GetT()
    {
        return varOne;
    }

    public U GetU()
    {
        return varTwo;
    }

    public V GetV()
    {
        return var3;
    }
}
