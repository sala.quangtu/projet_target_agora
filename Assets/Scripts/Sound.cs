﻿using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sound
{
    public string name = null;

    public string tag = null;

    public AudioClip[] clip = new AudioClip[0];

    [Range(0f, 1f)]
    public float volume = 1f;
    [Range(0.1f, 3f)]
    public float pitch = 1f;

    public bool playOnAwake = false;
    public bool loop = false;

    public AudioMixerGroup output = null;

    [HideInInspector]
    public AudioSource source = null;
}