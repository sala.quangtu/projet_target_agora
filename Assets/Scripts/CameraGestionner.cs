﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraGestionner : MonoBehaviour
{
    [SerializeField]
    SO_Camera so_cam = null;

    [SerializeField]
    InputField m_X = null;
    [SerializeField]
    InputField m_Y = null;
    [SerializeField]
    InputField m_Z = null;
    [SerializeField]
    InputField m_FOV = null;


    // Start is called before the first frame update
    void Start()
    {
        m_X.text = so_cam.X.ToString();
        m_X.onValueChanged.AddListener(delegate { so_cam.ActuX(m_X.text); });

        m_Y.text = so_cam.Y.ToString();
        m_Y.onValueChanged.AddListener(delegate { so_cam.ActuY(m_Y.text); });

        m_Z.text = so_cam.Z.ToString();
        m_Z.onValueChanged.AddListener(delegate { so_cam.ActuZ(m_Z.text); });

        m_FOV.text = so_cam.FOV.ToString();
        m_FOV.onValueChanged.AddListener(delegate { so_cam.ActuFOV(m_FOV.text); });
    }
}
