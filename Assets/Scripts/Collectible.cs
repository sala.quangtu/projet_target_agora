﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class Collectible : MonoBehaviour
{
    [Header("Events")]
    [SerializeField]
    private CollectibleEvent m_onSpawn = new CollectibleEvent();
    [SerializeField]
    private CollectibleEvent m_onCollected = new CollectibleEvent();

    [Space, Header("Variables")]
    [SerializeField, Tooltip("Defines the amount of score to get when collecting this object")]
    private int m_score = 100;

    [SerializeField, Tooltip("Defines the rotation speed")]
    private float m_rotationSpeed = 20;

    private Vector3 m_initPosition;

    [SerializeField, Tooltip("Defines the max distance")]
    private float m_maxDist = 20f;

    [SerializeField, Tooltip("Defines the distance every second")]
    private float m_dist = 2;

    private bool m_wasUp = false;
    private bool m_isLost = false;
    private bool m_canBeCollected = true;

    private bool _gotScorrer = false;

    CollectiblesGestionner _cg;
    [SerializeField, Tooltip("time before it can be collected (dropped coin only)")]
    private float m_time = 1f;
    private float m_t;
    //[SerializeField, Tooltip("For dropped coins")]
    //LayerMask groundLayer;
    private void Start()
    {
        try
        {
            _cg = FindObjectOfType<CollectiblesGestionner>();
            if(_cg == null)
            {
                _gotScorrer = false;
            }
            else
            {
                _gotScorrer = true;
            }
        }
        catch (System.Exception)
        {
            throw;
        }

        m_wasUp = false;
        m_initPosition = transform.position;
    }

    public void Spawn()
    {
        transform.DOPunchScale(Vector3.one, .75f,9,0);
        m_onSpawn.Invoke(new CollectibleInfo { position = m_initPosition });
    }

    // Defines if this collectible has been collected
    [SerializeField]
    private bool m_Collected = false;

    private void Update()
    {
        if(_gotScorrer)
        {
            m_score = _cg.SO_Collectible.Score;
            m_rotationSpeed = _cg.SO_Collectible.RotationSpeed;
            m_maxDist = _cg.SO_Collectible.MaxDistance;
            m_dist = _cg.SO_Collectible.Distance;
        }

        if (m_Collected)
        {
            return;
        }

        if(m_isLost)
        {

            var x = Mathf.Clamp(transform.position.x, -9.15f, 9.15f);
            transform.position = new Vector3(x, transform.position.y, transform.position.z);

        }
        transform.RotateAround(transform.position, Vector3.up, m_rotationSpeed * Time.deltaTime);

        if(transform.position.y <= m_initPosition.y + m_maxDist && !m_wasUp)
        {
            transform.Translate(Vector3.up * m_dist * Time.deltaTime);
        }

        if (transform.position.y >= m_initPosition.y - m_maxDist && m_wasUp)
        {
            transform.Translate(Vector3.up * -m_dist * Time.deltaTime);
        }

        if(transform.position.y > m_initPosition.y + m_maxDist)
        {
            m_wasUp = true;
        }

        if(transform.position.y <= m_initPosition.y - m_maxDist)
        {
            m_wasUp = false;
        }
        
    }

    ///<summary>
    /// Checks if this collectible has been collected.
    ///</summary>
    public bool IsCollected
    {
        get { return m_Collected; }
    }

    ///<summary>
    /// Gets the amount of score to get when collecting this object.
    ///</summary>
    public int Score
    {
        get { return m_score; }
    }

    ///<summary>
    /// Resets this collectible state (it can be collected).
    ///</summary>
    public void ResetState()
    {
        m_Collected = false;
    }

    ///<summary>
    /// Called when an object enters in this object's trigger.
    ///</summary>
    private void OnTriggerEnter(Collider _Other)
    {
        if (m_Collected) { return; }
        if (_Other.gameObject.tag == "Bumper")
        {
            m_canBeCollected = true;
            //Debug.Log("Rebond pieces");
            //var dir = _Other.transform.position - transform.position;
            //GetComponent<Rigidbody>().AddForce(dir *10, ForceMode.Impulse);
        }

        //if(_Other.gameObject.GetComponent<CharacterControllerP1>())
        if (_Other.gameObject.tag == "Player" && m_canBeCollected)
        {
            Debug.Log(_Other.GetComponent<CharacterControllerP1>());
            _Other.GetComponent<CharacterControllerP1>().OnCollect();
            m_onCollected.Invoke(new CollectibleInfo { position = transform.position });
            //Debug.Log("recup");
            m_Collected = true;
            Scorer _scorer = FindObjectOfType<Scorer>();
            _scorer.AddScore(m_score);
           
            if(!m_isLost)
            {
                CollectiblesGestionner _cg = FindObjectOfType<CollectiblesGestionner>();
                _cg.CollectibleCollected(this);
            }
            else
            {
                
                Destroy(this.gameObject);
            }
            
        }
    }

    public void ToggleLost()
    {
        m_isLost = !m_isLost;
    }
}
