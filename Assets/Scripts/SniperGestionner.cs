﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SniperGestionner : MonoBehaviour
{
    [SerializeField]
    SO_SniperInfo so_sniper = null;

    [SerializeField]
    InputField m_moveSpeed = null;
    [SerializeField]
    InputField m_reactivity = null;
    [SerializeField]
    InputField m_cooldown = null;
    [SerializeField]
    InputField m_propulsion = null;


    // Start is called before the first frame update
    void Start()
    {
        m_moveSpeed.text = so_sniper.moveSpeed.ToString();
        m_moveSpeed.onValueChanged.AddListener(delegate { so_sniper.ActuMoveSpeed(m_moveSpeed.text); });

        m_reactivity.text = so_sniper.reactivity.ToString();
        m_reactivity.onValueChanged.AddListener(delegate { so_sniper.ActuReactivity(m_reactivity.text); });

        m_cooldown.text = so_sniper.cooldown.ToString();
        m_cooldown.onValueChanged.AddListener(delegate { so_sniper.ActuCooldown(m_cooldown.text); });

        m_propulsion.text = so_sniper.propulsionForce.ToString();
        m_propulsion.onValueChanged.AddListener(delegate { so_sniper.ActuPropulsionForce(m_propulsion.text); });
    }
}
