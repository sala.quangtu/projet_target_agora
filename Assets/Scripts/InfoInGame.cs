﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoInGame : MonoBehaviour
{
    private Scorer m_scorer = null;
    private GameMaster m_gm = null;

    [SerializeField]
    private Text m_scoreText = null;
    [SerializeField]
    private Text m_hitText = null;
    private int m_initFontSize = 0;
    [SerializeField]
    private float m_duration = 0f;

    [Space, SerializeField]
    private Text m_timerText = null;
    [SerializeField]
    private Image m_timerSprite = null;

    private float m_timer = 0f;

    private IEnumerator coroutineScore = null;
    private IEnumerator coroutineHit = null;

    [SerializeField]
    private bool m_showTime = false;
    public bool showTime => m_showTime;
    public void SetShowTime(bool _value)
    {
        m_showTime = _value;
    }

    // Start is called before the first frame update
    void Start()
    {
        m_scorer = FindObjectOfType<Scorer>();
        m_gm = GameMaster.GM;
        m_timer = m_gm.RoundDuration + 2f;
        m_initFontSize = m_scoreText.fontSize;
        m_showTime = false;
    }

    // Update is called once per frame
    void Update()
    {
        m_timer = m_gm.RoundDuration - m_gm.Duration;
        m_timerText.text = Mathf.RoundToInt(m_timer).ToString();
        m_timerSprite.fillAmount = m_gm.Duration / m_gm.RoundDuration;
    }

    public void UpdateScore()
    {
        coroutineScore = MovingScore(m_duration);
        StopCoroutine(coroutineScore);
        m_scoreText.fontSize = m_initFontSize;
        coroutineScore = MovingScore(m_duration);
        m_scoreText.text = m_scorer.Score.ToString();
        StartCoroutine(coroutineScore);
    }

    public void UpdateOnlyScore()
    {
        m_scoreText.text = m_scorer.Score.ToString();
    }

    private IEnumerator MovingScore(float _duration)
    {
        float elapsed = 0.0f;
        float t = 0.0f;
        float d = _duration / 2;

        while (elapsed < d)
        {
            m_scoreText.fontSize = (int)Mathf.Lerp(m_initFontSize, m_initFontSize * 2, elapsed/d);

            elapsed += Time.deltaTime * 2;


            yield return null;
        }

        while(elapsed + t < _duration)
        {
            m_scoreText.fontSize = (int)Mathf.Lerp(m_initFontSize * 2, m_initFontSize, t/d);

            t += Time.deltaTime * 2;

            yield return null;
        }
    }

    public void UpdateHit()
    {
        coroutineHit = MovingHit(m_duration);
        StopCoroutine(coroutineHit);
        m_hitText.fontSize = m_initFontSize;
        coroutineHit = MovingHit(m_duration);
        m_hitText.text = "Hit : " + m_scorer.Hit;
        StartCoroutine(coroutineHit);
    }

    public void UpdateOnlyHit()
    {
        m_scoreText.text = "Hit : " + m_scorer.Hit;
    }

    private IEnumerator MovingHit(float _duration)
    {
        float elapsed = 0.0f;
        float t = 0.0f;
        float d = _duration / 2;

        while (elapsed < d)
        {
            m_hitText.fontSize = (int)Mathf.Lerp(m_initFontSize, m_initFontSize * 2, elapsed / d);

            elapsed += Time.deltaTime * 2;


            yield return null;
        }

        while (elapsed + t < _duration)
        {
            m_hitText.fontSize = (int)Mathf.Lerp(m_initFontSize * 2, m_initFontSize, t / d);

            t += Time.deltaTime * 2;

            yield return null;
        }
    }
}
