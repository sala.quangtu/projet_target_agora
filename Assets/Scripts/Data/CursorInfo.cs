﻿
using UnityEngine;

[System.Serializable]
public struct CursorInfo 
{
    public static readonly CursorInfo Empty = new CursorInfo();
    Vector3 cursorPos;
}
