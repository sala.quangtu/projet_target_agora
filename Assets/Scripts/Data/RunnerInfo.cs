﻿
using UnityEngine;

[System.Serializable]
public struct RunnerInfo 
{
    public static readonly RunnerInfo Empty = new RunnerInfo();
    Vector3 playerPos;
}
