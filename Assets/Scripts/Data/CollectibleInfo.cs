﻿using UnityEngine;

[System.Serializable]
public struct CollectibleInfo
{
    public Vector3 position;
}
