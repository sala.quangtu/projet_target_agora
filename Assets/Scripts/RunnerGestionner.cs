﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RunnerGestionner : MonoBehaviour
{
    [SerializeField]
    SO_RunnerInfo so_runner = null;

    [SerializeField]
    InputField m_moveSpeed = null;
    [SerializeField]
    InputField m_jumpHeight = null;
    [SerializeField]
    InputField m_gravity = null;
    [SerializeField]
    InputField m_reactivity = null;
    [SerializeField]
    Slider m_accelTime = null;
    [SerializeField]
    Text m_accelTimeText = null;
    [SerializeField]
    InputField m_deccelTime = null;
    [SerializeField]
    InputField m_bounceForce = null;

    // Start is called before the first frame update
    void Start()
    {
        m_moveSpeed.text = so_runner.moveSpeed.ToString();
        m_moveSpeed.onValueChanged.AddListener(delegate { so_runner.ActuMoveSpeed(m_moveSpeed.text); });

        m_jumpHeight.text = so_runner.jumpHeight.ToString();
        m_jumpHeight.onValueChanged.AddListener(delegate { so_runner.ActuJumpHeight(m_jumpHeight.text); });

        m_gravity.text = so_runner.gravity.ToString();
        m_gravity.onValueChanged.AddListener(delegate { so_runner.ActuGravity(m_gravity.text); });

        m_reactivity.text = so_runner.reactivity.ToString();
        m_reactivity.onValueChanged.AddListener(delegate { so_runner.ActuReactivity(m_reactivity.text); });

        m_accelTime.value = so_runner.accelTime;
        m_accelTimeText.text = so_runner.accelTime.ToString();
        m_accelTime.onValueChanged.AddListener(delegate { so_runner.ActuAccelTime(m_accelTime.value); });
        m_accelTime.onValueChanged.AddListener(delegate { ActuAccelTimeText(); });

        m_deccelTime.text = so_runner.deccelTime.ToString();
        m_deccelTime.onValueChanged.AddListener(delegate { so_runner.ActuDeccelTime(m_deccelTime.text); });

        m_bounceForce.text = so_runner.bounceForce.ToString();
        m_bounceForce.onValueChanged.AddListener(delegate { so_runner.ActuBounceForce(m_bounceForce.text); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ActuAccelTimeText()
    {
        m_accelTimeText.text = so_runner.accelTime.ToString();
    }
}