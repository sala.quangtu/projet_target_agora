﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    Vector3 direction;
    Vector3 basePos;
    Vector3 target;
    float speed;
    float distance;
    float actuDist;

    AnimationCurve curve;
    // Start is called before the first frame update
    void Start()
    {
        basePos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, basePos) >= distance)
        {
            Destroy(transform.gameObject);
        }
        else
        {

            transform.position = transform.position + direction * speed * Time.deltaTime ;
            actuDist = 1 - (Vector3.Distance(transform.position, target) / distance);
            //var te = Mathf.Lerp(actuDist, distance, speed);
             transform.GetChild(0).transform.position = new Vector3(transform.position.x, transform.position.y + curve.Evaluate(actuDist), transform.position.z);

            Debug.Log("Dist : " + actuDist);
        }
    }

    public void SetValue(Vector3 dir, float spd, float dist, Vector3 tar, AnimationCurve c)
    {
        direction = dir;
        speed = spd;
        distance = dist;
        transform.rotation = Quaternion.LookRotation(direction);
        target = tar;
        curve = c;
    }
}
