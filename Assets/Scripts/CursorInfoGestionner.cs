﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorInfoGestionner : MonoBehaviour
{
    [SerializeField]
    SO_Cursor so_cursor = null;

    [SerializeField]
    InputField m_dist = null;
    [SerializeField]
    InputField m_range = null;
    [SerializeField]
    InputField m_maxRange = null;

    private void Start()
    {
        m_dist.text = so_cursor.Dist.ToString();
        m_dist.onValueChanged.AddListener(delegate { so_cursor.ActuDist(m_dist.text); });

        m_range.text = so_cursor.Range.ToString();
        m_range.onValueChanged.AddListener(delegate { so_cursor.ActuRange(m_range.text); });

        m_maxRange.text = so_cursor.MaxRange.ToString();
        m_maxRange.onValueChanged.AddListener(delegate { so_cursor.ActuMaxRange(m_maxRange.text); });
    }
}
