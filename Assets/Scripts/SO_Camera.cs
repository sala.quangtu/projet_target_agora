﻿using UnityEngine;

[CreateAssetMenu(fileName = "SO_Camera", menuName = "ScriptableObject/SO_Camera", order = 150)]
public class SO_Camera : ScriptableObject
{
    public float X;
    public float Y;
    public float Z;
    public float FOV;

    [Space]
    public float initX;
    public float initY;
    public float initZ;
    public float initFOV;

    public void ActuX(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            X = result;
        }
    }

    public void ActuY(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            Y = result;
        }
    }

    public void ActuZ(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            Z = result;
        }
    }

    public void ActuFOV(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            FOV = result;
        }
    }
}