﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GestionerMenu : MonoBehaviour
{
    [SerializeField]
    private Button m_startButton = null;

    private void Start()
    {
        m_startButton.onClick.AddListener(delegate { GameMaster.GM.StartGame(); });
    }

    public void CloseTheGame()
    {
        Application.Quit();
    }
}