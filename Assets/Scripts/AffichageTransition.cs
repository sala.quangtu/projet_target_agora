﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AffichageTransition : MonoBehaviour
{
    [SerializeField]
    Text m_score = null;

    [SerializeField]
    Text m_hit = null;

    [SerializeField]
    Text m_timer = null;
    float m_t = 0f;

    GameMaster m_gm = null;

    private void Awake()
    {
        m_gm = FindObjectOfType<GameMaster>();
    }


    // Start is called before the first frame update
    void Start()
    {
        m_score.text = "P1 : " + GameMaster.GM.ScoreP1;
        m_hit.text = "Hit P2 : " + GameMaster.GM.HitP2;
        m_t = GameMaster.GM.ScoreDuration +2f;
    }

    void Update()
    {
        m_t = m_gm.ScoreDuration - m_gm.Duration;
        m_timer.text = Mathf.RoundToInt(m_t).ToString();
    }
}