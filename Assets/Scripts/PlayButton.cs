﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButton : MonoBehaviour
{
    [SerializeField]
    private bool m_runnerOn = false;
    public bool RunnerOn { get { return m_runnerOn; } }

    [Space, SerializeField]
    private bool m_hasTouched = false;
    public bool HasTouched { get { return m_hasTouched; } }
    public void SetHasTouched(bool _value)
    {
        m_hasTouched = _value;
    }

    private void Update()
    {
        if (m_runnerOn && m_hasTouched)
        {
            GameMaster.GM.StartGame();
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            m_runnerOn = true;
        }
    }

    //private void OnTriggerExit(Collider other)
    //{
    //    if(other.tag == "Player")
    //    {
    //        m_runnerOn = false;
    //    }
    //}
}