﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour
{
    public static GameMaster GM;
    private void Awake()
    {
        GameMaster[] objs = FindObjectsOfType<GameMaster>();
        if(objs.Length > 1)
        {
            Destroy(objs[1].gameObject);
        }

        GM = this;
        DontDestroyOnLoad(GM.gameObject);
    }

    [HideInInspector]
    public int FirstScene = 0;

    private bool m_phase1 = true;
    public bool Phase1
    {
        get { return m_phase1; }
    }

    private int m_scoreP1 = 0;
    public int ScoreP1
    {
        get { return m_scoreP1; }
    }

    private int m_hitP1 = 0;
    public int HitP1
    {
        get { return m_hitP1; }
    }

    private int m_scoreP2 = 0;
    public int ScoreP2
    {
        get { return m_scoreP2; }
    }
    private int m_hitP2 = 0;
    public int HitP2
    {
        get { return m_hitP2; }
    }

    public bool ONE_SCENE = true;
    public bool TWO_SCENE = false;
    public bool THREE_SCENE = false;

    [Space, SerializeField]
    private bool m_zaWarldo = false;
    public bool ZaWarldo { get { return m_zaWarldo; } }

    [SerializeField]
    private float m_duration = 0f;
    public float Duration => m_duration;

    [Space, SerializeField]
    private float m_roundDuration = 60f;
    public float RoundDuration { get { return m_roundDuration; } }

    [SerializeField]
    private float m_scoreDuration = 10f;
    public float ScoreDuration { get { return m_scoreDuration; } }

    

    private void Update()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Glace")
            || SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Automne")
            || SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Neon")
            || SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Vegetation"))
        {
            if (Input.GetKeyDown(KeyCode.O))
            {
                if (m_zaWarldo)
                {
                    m_zaWarldo = false;
                    IEnumerator coroutine = WaitFor(m_roundDuration, m_duration);
                    StartCoroutine(coroutine);
                }
                else
                {
                    m_zaWarldo = true;
                    StopAllCoroutines();
                }
            }
        }
    }

    public void StartGame()
    {
        //Debug.Log("debut du jeu");

        m_scoreP1 = 0;
        m_scoreP2 = 0;
        m_hitP1 = 0;
        m_hitP2 = 0;
        m_phase1 = true;
        m_zaWarldo = false;
        m_duration = 0f;

        FindObjectOfType<Transition>().FirstLevelScene();
    }

    public void LaunchRound()
    {
        //Debug.Log("Debut du round");
        IEnumerator coroutine = WaitFor(m_roundDuration, 0f);
        StartCoroutine(coroutine);
    }

    public void EndRound()
    {
        //Debug.Log("endRound");
        Scorer _score = FindObjectOfType<Scorer>();
        if(Phase1)
        {
            m_scoreP1 = _score.Score;
            m_hitP2 = _score.Hit;
            m_phase1 = false;
            _score.ResetScore();
            FindObjectOfType<Transition>().TransitionScene();
        }
        else
        {
            m_scoreP2 = _score.Score;
            m_hitP1 = _score.Hit;
            _score.ResetScore();
            FindObjectOfType<Transition>().EndScene();
        }
    }

    public void BetweenLevels()
    {
        IEnumerator coroutine = BetweenLevelsWait(m_scoreDuration);
        StartCoroutine(coroutine);
    }

    //private IEnumerator WaitFor(float _waitTime)
    //{
    //    yield return new WaitForSeconds(_waitTime);
    //    EndRound();
    //}

    private IEnumerator WaitFor(float _waitTime, float _alredyWaited)
    {
        float _duration = _alredyWaited;


        while(_duration < _waitTime)
        {
            _duration += Time.deltaTime;
            m_duration = _duration;
            yield return null;
        }

        if(_duration > _waitTime)
        {
            EndRound();
        }
    }

    private IEnumerator BetweenLevelsWait(float _waitTime)
    {
        float _duration = 0f;

        while (_duration < _waitTime)
        {
            _duration += Time.deltaTime;
            m_duration = _duration;
            yield return null;
        }

        if (_duration > _waitTime)
        {
            FindObjectOfType<Transition>().SecondLevelScene(FirstScene);
        }
    }
}