﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;
using UnityEngine.SceneManagement;
public class AimController : MonoBehaviour
{
    public SO_SniperInfo Info;
    public float moveSpeed;

    public float slowedSpeed;
    public float slowTime;
    float slowt;

    bool isSlowed;
    float speed;
    //public float jumpHeight;
    //public float gravity;
    private Player player;
    public float reactivity;
    public float accelTime;
    float accel;
    public float deccelTime;
    public float cooldDown;

    public float wallDist;
    public float size;
     float cooltime;
    float deccel;

    Vector3 dir;
    Vector3 finalDir;
    Vector3 sideDir;
    //Vector3 velocity;
    float x;
    float y;
    //bool grounded;
    bool collided;
    bool sideCollided;
    bool statique;
    //bool jump;
    //float timeJump;
    //public GameObject projectile;
    //public GameObject launcher;

    public LayerMask collisionLayer;
    //public LayerMask playerLayer;
    //public LayerMask groundLayer;
    //public GameObject wallHitPart;
    //public GameObject playerHitPart;


    //public ElevateBricks eb;

    //public Text scoreText;
    //public int score;

    Vector3 wantedCheckDir;
    //public float groundDist;
    public bool activate = false;


    //Shoot Area
    //public float projectileSpeed;
    //public AnimationCurve heightCurve;
    public float coolDown;
    float coolTime;
    public int onHitScoreLoose;
    //public int ammoammount;
    //public bool isHeat;
    //public GameObject projectile;
    //public GameObject launcher;
    public LayerMask target;
    //public ParticleSystem playerHitParticule;
    //public ParticleSystem wallHitParticule;

    //private Player player;
    Scorer score;
    private GameObject ply;
    private MovingCursor mc;
    private ElevateBricks eb;
    float invT;
    float it;
    bool hasHit;
    //public GameObject hit;
    //public GameObject miss;
    // Start is called before the first frame update

    [Header("Events")]
    [SerializeField]
    private cursorEvents _onShoot = new cursorEvents();
    [SerializeField]
    private cursorEvents _onHit = new cursorEvents();
    [SerializeField]
    private cursorEvents _onMiss = new cursorEvents();

    public void ToggleActivate()
    {
        activate = true;
    }

    private void Start()
    {
        //Debug.Log("starts start");
        activate = false;
        //Debug.Log("je vais recupérer le reimpurt de ses mort qui me fait chier sa mère et qui fait tout cassé");
        player = ReInput.players.GetPlayer("Player2");
        //Debug.Log("ReIput");
        eb = FindObjectOfType<ElevateBricks>();
        //Debug.Log("LesBriques");
        ply = GameObject.FindGameObjectWithTag("Player");
        //Debug.Log("LeJoueurRunner");
        //if (ply)
        //{
        //    it = ply.GetComponent<CharacterControllerP1>().invulnerabilityTime;
        //    Debug.Log("LeTempsInvul");
        //}
        //else Debug.LogError("IL EXISTE PAS PTN");
        mc = FindObjectOfType<MovingCursor>();
        //Debug.Log("MovingCursor");
        score = GameObject.FindObjectOfType<Scorer>();
        //Debug.Log("Scorer");
        //player = ReInput.players.GetPlayer("Player2");
    }
    public void Update()
    {
        moveSpeed = Info.moveSpeed;
        reactivity = Info.reactivity;
        cooldDown = Info.cooldown;
        if (ply == null)
        {
            ply = GameObject.FindGameObjectWithTag("Player");
            it = ply.GetComponent<CharacterControllerP1>().invulnerabilityTime;
        }
        //CheckGround(); 
        if (activate)
        {
            y = Mathf.Clamp(transform.position.y, -4.2f, 3);
            x = Mathf.Clamp(transform.position.x, -9.15f, 9.15f);
            CheckCapsuleDirCollision();
            transform.position = new Vector3(x, y, transform.position.z);
            //ApplyGravity();
            /*var dir = */
            GetInput();
            GetSpeed();

            if (!collided) Move(finalDir.normalized);
            else if (!sideCollided && !statique) MoveAlong(sideDir);
            if(isSlowed)
            {
                if (slowt < slowTime)
                {
                    slowt += Time.deltaTime;
                }
                else
                {
                    isSlowed = false;
                    slowt = 0;
                }
            }

            //if (player.GetButtonDown("Shoot") && )
            //{
            //    //Debug.Log("SHOOT BUTTON PRESSED");
            //    Shoot();
            //}
            else if (player.GetButton("Shoot") && coolTime == 0) Shoot();
            if (coolTime > 0) StartCoolDown();

            if (hasHit)
            {

                if (invT < it)
                {
                    invT += Time.deltaTime;
                }
                else
                {
                    //if(ply != null)
                    //{
                    //    ply.GetComponent<CharacterControllerP1>().isInvulnerable = false;
                    //}
                    hasHit = false;
                    invT = 0;
                }
            }
        }
        //if (player.GetButton("Shoot") && cooltime == 0) Shoot();
        //if (player.GetButtonDown("Shoot")) Shoot();
        //if (cooltime > 0) StartCoolDown();


        //scoreText.text = "player 2 score : " + score;
    }
    void StartCoolDown()
    {
        if (coolTime > 0)
        {
            coolTime -= Time.deltaTime;
        }
        if (coolTime <= 0) coolTime = 0;
    }

    void Shoot()
    {
        mc.Shoot();
        Debug.DrawLine(transform.position, transform.position + transform.forward * 10f, Color.red);
        _onShoot.Invoke(CursorInfo.Empty);
        Collider[] go = Physics.OverlapSphere(transform.position + transform.forward * 0.5f, 0.5f, target);
        //Debug.Log("SHOOT");
        if (go.Length > 0)
        {
            //Debug.Log("HAS HIT PLAYER");
            _onHit.Invoke(CursorInfo.Empty);
            //GameObject ob = Instantiate(hit) as GameObject;
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Menu2"))
            {
                GameObject.FindObjectOfType<PlayButton>().SetHasTouched(true);
            }
            else
            {
               
                if (!hasHit)
                {
                    //if(ply == null)
                    //{
                    //    foreach (var g in go)
                    //    {
                    //        if(g.GetComponent<CharacterControllerP1>())
                    //        {
                    //            ply = g.gameObject;
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    it = ply.GetComponent<CharacterControllerP1>().invulnerabilityTime;
                    //    ply.GetComponent<CharacterControllerP1>().isInvulnerable = true;
                    //}
                    score.SubstractScore(ply.transform.position, onHitScoreLoose);
                    transform.GetComponent<AimController>().ToggleSlowed();
                    hasHit = true;
                    accel = 0;
                    //Debug.Log(go.Length);
                }


            }
        }
        else
        {
            //Debug.Log("HAS HIT WALL");
            _onMiss.Invoke(CursorInfo.Empty);
            //GameObject ob = Instantiate(miss) as GameObject;
            eb.AddWave(transform.position);
        }

        //GameObject go = Instantiate(projectile) as GameObject;
        //go.transform.position = launcher.transform.position;
        //go.AddComponent<Projectile>();
        //var dir = this.transform.position - launcher.transform.position;
        //var dist = Vector3.Distance(transform.position,new Vector3(transform.position.x, transform.position.y, 0));
        //go.GetComponent<Projectile>().SetValue(dir , projectileSpeed, dist, transform.position, heightCurve );
        //go.transform.parent = launcher.transform;
        coolTime = coolDown;
        //Debug.DrawLine(launcher.transform.position, transform.position, Color.cyan);
    }


    public void ToggleSlowed()
    {
        isSlowed = true;
    }
    void GetInput()
    {
        x = player.GetAxisRaw("MoveHorizontalP2");
        y = player.GetAxisRaw("MoveVerticalP2");
        dir = transform.right * x + transform.up * y;
        finalDir = Vector3.Lerp(finalDir, dir, reactivity * Time.deltaTime);
        //if (x == 0 && y == 0) finalDir = Vector3.zero;
        if (!collided) sideDir = Vector3.zero;

        //return usedDir;
    }
    void GetSpeed()
    {

        if (x == 0 && y == 0)
        {
            //float rate = 1.0f / deccelTime;
            //if (deccel > 0)
            //{
            //    deccel -= Time.deltaTime * rate;
            //}
            speed = Mathf.Lerp(speed, 0, deccelTime * Time.deltaTime);
            if (speed < 0.05f) speed = 0;
        }
        else
        {
            if(isSlowed)
            {
                speed = slowedSpeed;

            }
            else speed = moveSpeed;
        }
    }
    void Move(Vector3 dir)
    {
        if (x != 0 || y != 0)
        {
            float rate = 1.0f / accelTime;

            if (accel < 1)
            {
                accel += Time.deltaTime * rate;
            }
        }
        else accel = 0;
        transform.position += dir * (speed * accel) * Time.deltaTime;
    }

    void MoveAlong(Vector3 dir)
    {
        //if (x != 0 || y != 0)
        //{
        //    float rate = 1.0f / accelTime;

        //    if (accel < 1)
        //    {
        //        accel += Time.deltaTime * rate;
        //    }
        //}

        transform.position += dir * (speed * accel) * Time.deltaTime;
    }
    void CheckCapsuleDirCollision()
    {
        #region old
        //if (collided)
        //{
        //    RaycastHit hit2;

        //    if (Physics.BoxCast(transform.position, size, usedDir.normalized, out hit2, Quaternion.identity, wallDist + 0.15f, collisionLayer))
        //    {
        //        sideCollided = true;
        //    }
        //        //else sideCollided = false;

        //        //Debug.DrawLine(transform.position, transform.position + usedDir);
        //}
        //else sideCollided = false;

        //RaycastHit hit;
        ////var dir = Vector3.zero;
        //if (Mathf.Abs(x) > Mathf.Abs(y) && Mathf.Abs(y) < 0.1f)
        //{
        //    if (x > 0) wantedCheckDir = new Vector3(1, 0, 0);
        //    else wantedCheckDir = new Vector3(-1, 0, 0);
        //}
        //else if (Mathf.Abs(y) > Mathf.Abs(x) && Mathf.Abs(x) < 0.1f)
        //{
        //    if (y > 0) wantedCheckDir = new Vector3(0, 1, 0);
        //    else wantedCheckDir = new Vector3(0, -1, 0);
        //}
        //else
        //{
        //    wantedCheckDir = new Vector3(Mathf.RoundToInt(x), Mathf.RoundToInt(y), 0);
        //}
        ////else check x y dir;
        //if (Physics.BoxCast(transform.position, size, wantedCheckDir , out hit, Quaternion.identity, wallDist + 0.15f, collisionLayer))
        //{
        //    collided = true;
        //    //Debug.Log("Colllllllide");


        //    if (!sideCollided)
        //    {

        //        if (Vector3.Dot(hit.normal, Vector3.up) != 0)
        //        {
        //            //Debug.Log("Hit Ceilling or ground");
        //            var dot = Vector3.Dot(finalDir, Vector3.right);
        //            if (dot > 0.1f)
        //            {
        //                //Debug.Log("right");
        //                usedDir = Vector3.right;
        //                statique = false;
        //            }
        //            else if (dot < -0.1f)
        //            {
        //                //Debug.Log("left");
        //                usedDir = -Vector3.right;
        //                statique = false;
        //            }
        //            else
        //            {
        //                usedDir = Vector3.zero;
        //                statique = true;
        //            }
        //        }
        //        if (Vector3.Dot(hit.normal, Vector3.right) != 0)
        //        {
        //            //Debug.Log("Hit Wall");
        //            var dot = Vector3.Dot(finalDir, Vector3.up);
        //            if (dot > 0.1f)
        //            {

        //                //Debug.Log("up");
        //                usedDir = Vector3.up;
        //                statique = false;
        //            }
        //            else if (dot < -0.1f)
        //            {
        //                //Debug.Log("^down");
        //                usedDir = -Vector3.up;
        //                statique = false;
        //            }
        //            else
        //            {
        //                usedDir = Vector3.zero;
        //                statique = true;
        //            }
        //        }

        //    }


        //    //if (Vector3.Dot(finalDir, Vector3.up) > 0) Debug.Log("Up");
        //    //else if (Vector3.Dot(finalDir, Vector3.up) < 0) Debug.Log("Down");
        //    //if (Vector3.Dot(finalDir, Vector3.up) > 0) Debug.Log("left");
        //    //else if (Vector3.Dot(finalDir, Vector3.up) < 0) Debug.Log("right");


        //}

        //else
        //{
        //    collided = false;
        //    statique = false;
        //}
        #endregion
        #region collision

        if (Physics.SphereCast(transform.position, size, finalDir.normalized, out RaycastHit hit, wallDist + 0.15f, collisionLayer))
        {
            collided = true;
            wantedCheckDir = -hit.normal;
            if (Vector3.Dot(hit.normal, Vector3.up) != 0)
            {
                //Debug.Log("Hit Ceilling or ground");
                var dot = Vector3.Dot(finalDir, Vector3.right);
                if (dot > 0.1f)
                {
                    //Debug.Log("right");
                    sideDir = Vector3.right;
                    //statique = false;
                }
                else if (dot < -0.1f)
                {
                    //Debug.Log("left");
                    sideDir = -Vector3.right;
                    //statique = false;
                }
                else
                {
                    sideDir = Vector3.zero;
                    //statique = true;
                }
            }
            if (Vector3.Dot(hit.normal, Vector3.right) != 0)
            {
                //Debug.Log("Hit Wall");
                var dot = Vector3.Dot(finalDir, Vector3.up);
                if (dot > 0.1f)
                {

                    //Debug.Log("up");
                    sideDir = Vector3.up;
                    //statique = false;
                }
                else if (dot < -0.1f)
                {
                    //Debug.Log("down");
                    sideDir = -Vector3.up;
                    //statique = false;
                }
                else
                {
                    sideDir = Vector3.zero;
                    //statique = true;
                }
            }


        }
        else collided = false;
        if (Physics.SphereCast(transform.position, size, sideDir.normalized, out RaycastHit hitS, wallDist + 0.15f, collisionLayer))
        {
            sideCollided = true;
            if (Vector3.Distance(transform.position, hitS.point) < wallDist)
            {
                transform.position = Vector3.Lerp(transform.position, transform.position + hitS.normal * wallDist, 15f * Time.deltaTime);
            }
        }
        else sideCollided = false;
        Ray rayC = new Ray(transform.position, wantedCheckDir);
        RaycastHit hitC;
        if (Physics.Raycast(rayC, out hitC, wallDist + 0.15f, collisionLayer))
        {
            if (Vector3.Distance(transform.position, hitC.point) < wallDist)
            {
                transform.position = Vector3.Lerp(transform.position, transform.position + hitC.normal * wallDist, 15f * Time.deltaTime);
            }

        }
        
       // Debug.Log("Collided : " + collided);
        #endregion
        //Debug.DrawLine(transform.position, transform.position + finalDir * 2f, Color.black);
    }

    //void Shoot()
    //{
    //    DetectPlayer();
    //    GameObject go = Instantiate(projectile) as GameObject;
    //    go.transform.position = launcher.transform.position;
    //    //go.AddComponent<Projectile>();
    //    //go.GetComponent<Projectile>().SetDir(this.transform.position - launcher.transform.position);
    //    cooltime = cooldDown;
    //    Debug.DrawLine(launcher.transform.position,  transform.position - launcher.transform.position, Color.cyan);
    //}

    //void DetectPlayer()
    //{
    //    Ray ray = new Ray(launcher.transform.position, transform.position - launcher.transform.position);
    //    RaycastHit hit;
    //    if (Physics.SphereCast(ray, 0.5f, out hit, 50f, playerLayer))
    //    {
    //        if (hit.collider.tag == "Player")
    //        {
    //            Debug.Log("Hit Player");
    //            score++;
    //            GameObject go = Instantiate(playerHitPart) as GameObject;
    //            go.transform.position = transform.position;

    //        }
    //        if (hit.collider.tag == "Surface")
    //        {
    //            //eb.AddWave(transform.position);
    //            Debug.Log("Hit Surface");
    //            GameObject go = Instantiate(wallHitPart) as GameObject;
    //            go.transform.position = transform.position;
    //        }
    //    }
    //}

    //void StartCoolDown()
    //{
    //    if (cooltime > 0)
    //    {
    //        cooltime -= Time.deltaTime;
    //    }
    //    if (cooltime <= 0) cooltime = 0;
    //}
    private void OnDrawGizmos()
    {
        //Gizmos.color = Color.magenta;

        //Gizmos.DrawWireSphere(transform.position + finalDir * wallDist, size);
        //if(collided)
        //{
        //    Gizmos.color = Color.cyan;

        //    Gizmos.DrawWireSphere(transform.position + sideDir * wallDist, size);
        //}

    }
}
