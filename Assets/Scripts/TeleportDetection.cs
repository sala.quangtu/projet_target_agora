﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class TeleportDetection : MonoBehaviour
{
    static bool used;
    static bool hasBeenUsed;
    bool wait;
    TeleportSpawner ts;
    static CharacterControllerP1 cc;
    float time = 0;
    static float timet = 0;
    Vector3 targetTele;

    [SerializeField]
    private RunnerEvents _onTeleport = new RunnerEvents();
    [SerializeField]
    private RunnerEvents _onWait = new RunnerEvents();
    private void Start()
    {

        //cc = GameObject.FindGameObjectWithTag("Player").transform.GetComponent<CharacterControllerP1>();
        ts = GameObject.FindObjectOfType<TeleportSpawner>();
        this.gameObject.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Trigger tp");
        if (other.tag == "Player" && used == false)
        {
            if(cc == null)
            {
                cc = other.GetComponent<CharacterControllerP1>();
            }
            wait = true;
            used = true;
            _onWait.Invoke(RunnerInfo.Empty);
            //Debug.Log("start wait teleport");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player" && !hasBeenUsed)
        {
            used = false;
            wait = false;
            time = 0;
            timet = 0;
            hasBeenUsed = false;
            //Debug.Log("reset teleport time");
        }
    }
    private void Update()
    {

            if (used)
            {
                if (wait && !hasBeenUsed)
                {
                    if (timet > ts.teleportationDelay)
                    {
                        targetTele = ts.GetTargetTeleporter(transform.gameObject);
                        //TELEPORT
                        //Debug.Log(cc);
                    hasBeenUsed = true;
                    cc.TeleportPlayer(targetTele);
                        //Debug.Log("TELEPORT NOW");
                    _onTeleport.Invoke(RunnerInfo.Empty);
                    timet = 0;
                        wait = false;
      
                    }
                    else
                    {
                        timet += Time.deltaTime;
                    }
                }
                else
                {
                    var tbd = ts.timeBeforeDestroy;
                    if (time > tbd)
                    {
                        used = false;
                        ts.ResetTeleporter();
                        hasBeenUsed = false;
                        time = 0;

                    }
                    else
                    {
                        time += Time.deltaTime;
                   // this.GetComponent<MeshRenderer>().material.color = new Color(this.GetComponent<MeshRenderer>().material.color.r, this.GetComponent<MeshRenderer>().material.color.g, this.GetComponent<MeshRenderer>().material.color.b, this.GetComponent<MeshRenderer>().material.color.a - 0.75f);
                    }
                }
            }
            else
            {
                time = 0;
                timet = 0;
            }
        
        
    }
    public void SetTs(TeleportSpawner t)
    {
        ts = t;
    }

}
