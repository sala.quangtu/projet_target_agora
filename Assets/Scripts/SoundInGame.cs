﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundInGame : MonoBehaviour
{
    [SerializeField]
    SO_SoundGestionner so_sound = null;

    [SerializeField]
    InputField m_Master = null;
    [SerializeField]
    InputField m_Music = null;
    [SerializeField]
    InputField m_Rideaux = null;
    [SerializeField]
    InputField m_Coin = null;
    [SerializeField]
    InputField m_Hit = null;
    [SerializeField]
    InputField m_Deplacement = null;
    [SerializeField]
    InputField m_Portails = null;
    [SerializeField]
    InputField m_Game = null;


    void Start()
    {
        m_Master.text = so_sound.Master.ToString();
        m_Master.onValueChanged.AddListener(delegate { so_sound.ActuMaster(m_Master.text); });

        m_Music.text = so_sound.Music.ToString();
        m_Music.onValueChanged.AddListener(delegate { so_sound.ActuMusic(m_Music.text); });

        m_Rideaux.text = so_sound.Rideaux.ToString();
        m_Rideaux.onValueChanged.AddListener(delegate { so_sound.ActuRideaux(m_Rideaux.text); });

        m_Coin.text = so_sound.Coins.ToString();
        m_Coin.onValueChanged.AddListener(delegate { so_sound.ActuCoins(m_Coin.text); });

        m_Hit.text = so_sound.Hit.ToString();
        m_Hit.onValueChanged.AddListener(delegate { so_sound.ActuHit(m_Hit.text); });

        m_Deplacement.text = so_sound.Deplacement.ToString();
        m_Deplacement.onValueChanged.AddListener(delegate { so_sound.ActuDeplacement(m_Deplacement.text); });

        m_Portails.text = so_sound.Portails.ToString();
        m_Portails.onValueChanged.AddListener(delegate { so_sound.ActuPortails(m_Portails.text); });

        m_Game.text = so_sound.Game.ToString();
        m_Game.onValueChanged.AddListener(delegate { so_sound.ActuGame(m_Game.text); });
    }
}