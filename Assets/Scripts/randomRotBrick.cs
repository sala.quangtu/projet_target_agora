﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class randomRotBrick : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        Vector3 rot = new Vector3(90f - Random.Range(-3.0f, 3.0f), Random.Range(-3.0f, 3.0f), Random.Range(-3.0f, 3.0f));
        Vector3 pos = new Vector3(transform.localPosition.x,transform.localPosition.y, Random.Range(-0.05f, 0.05f));
        transform.localEulerAngles = rot;
        transform.localPosition = pos;
    }

}
