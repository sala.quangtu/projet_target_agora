﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectiblesInfoGestionner : MonoBehaviour
{
    [SerializeField]
    SO_Collectible so_collectible = null;

    [SerializeField]
    InputField m_score = null;
    [SerializeField]
    InputField m_rotation = null;
    [SerializeField]
    InputField m_maxDist = null;
    [SerializeField]
    InputField m_dist = null;

    private void Start()
    {
        m_score.text = so_collectible.Score.ToString();
        m_score.onValueChanged.AddListener(delegate { so_collectible.ActuScore(m_score.text); });

        m_rotation.text = so_collectible.RotationSpeed.ToString();
        m_rotation.onValueChanged.AddListener(delegate { so_collectible.ActuRotationSpeed(m_rotation.text); });

        m_maxDist.text = so_collectible.MaxDistance.ToString();
        m_maxDist.onValueChanged.AddListener(delegate { so_collectible.ActuMaxDistance(m_maxDist.text); });

        m_dist.text = so_collectible.Distance.ToString();
        m_dist.onValueChanged.AddListener(delegate { so_collectible.ActuDistance(m_dist.text); });
    }
}
