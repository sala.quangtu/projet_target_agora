﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Debugger", menuName = "ScriptableObject/Debugger", order = 150)]
public class NewDebugLogger : ScriptableObject
{
    public void DebugLog(string text)
    {
        Debug.Log(text);
    }

    public void ErrorLog(string text)
    {
        Debug.LogError(text);
    }

    public void WarningLog(string text)
    {
        Debug.LogWarning(text);
    }
}