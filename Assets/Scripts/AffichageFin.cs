﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AffichageFin : MonoBehaviour
{
    [SerializeField]
    Text m_scoreP1 = null;
    [SerializeField]
    Text m_hitP1 = null;
    [SerializeField]
    Text m_coinsP1 = null;

    [SerializeField]
    Text m_scoreP2 = null;
    [SerializeField]
    Text m_hitP2 = null;
    [SerializeField]
    Text m_coinsP2 = null;

    [SerializeField]
    GameObject m_egalite = null;

    [SerializeField]
    GameObject m_P1Win = null;

    [SerializeField]
    GameObject m_P2Win = null;


    // Start is called before the first frame update
    void Start()
    {
        m_scoreP1.text = "Score P1 : " + (GameMaster.GM.ScoreP1 + GameMaster.GM.HitP1);
        m_scoreP2.text = "Score P2 : " + (GameMaster.GM.ScoreP2 + GameMaster.GM.HitP2);
        m_hitP1.text = "Hit P1 : " + GameMaster.GM.HitP1;
        m_hitP2.text = "Hit P2 : " + GameMaster.GM.HitP2;
        m_coinsP1.text = "P1 : " + GameMaster.GM.ScoreP1;
        m_coinsP2.text = "P2 : " + GameMaster.GM.ScoreP2;

        if (GameMaster.GM.ScoreP1 + GameMaster.GM.HitP1 > GameMaster.GM.ScoreP2 + GameMaster.GM.HitP2)
        {
            m_egalite.SetActive(false);
            m_P1Win.SetActive(true);
            m_P2Win.SetActive(false);
        }
        else if (GameMaster.GM.ScoreP1 + GameMaster.GM.HitP1 < GameMaster.GM.ScoreP2 + GameMaster.GM.HitP2)
        {
            m_egalite.SetActive(false);
            m_P1Win.SetActive(false);
            m_P2Win.SetActive(true);
        }
        else
        {
            m_egalite.SetActive(true);
            m_P1Win.SetActive(false);
            m_P2Win.SetActive(false);
        }
    }
}