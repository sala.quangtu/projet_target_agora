﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Grid : MonoBehaviour
{
    [SerializeField]
    private Transform parent = null;

    [SerializeField]
    private GameObject m_PrefabToSpawn = null;

    [SerializeField]
    private int m_NbInstances = 10;

    [SerializeField]
    private float m_TranslationOffset = 1f;

    private Vector3 m_LastSpawnPosition = Vector3.zero;

    public float heigth;

    public bool Generate;

    public bool OnMoove;

    public Vector3 zone;

    public Vector3 size;

    public List<List<GameObject>> grid = new List<List<GameObject>>();

    private void Update()
    {
        zone = transform.lossyScale;

        if (Vector3.Distance(m_LastSpawnPosition, transform.position) > m_TranslationOffset && OnMoove)
        {
            SpawnPrefabs();
            m_LastSpawnPosition = transform.position;
        }

        if (Generate)
        {
            SpawnPrefabs();
        }
        Generate = false;
    }

    private void SpawnPrefabs()
    {
        for (int i = 0; i < zone.x; i++)
        {
            grid.Add(new List<GameObject>());
            for (int j = 0; j < zone.y; j++)
            {
                Vector3 targetPosition;

                if (j % 2 == 0)
                {
                    targetPosition = new Vector3
                    (
                        transform.position.x - zone.x / 2 + ((size.x * 2) * (i + 1) + 0.5f),
                        heigth,
                        transform.position.y - zone.y + (size.y * (j + 1))
                    );
                }
                else
                {
                    targetPosition = new Vector3
                    (
                        transform.position.x - zone.x / 2 + ((size.x * 2) * (i + 1)),
                        heigth,
                        transform.position.y - zone.y + (size.y * (j + 1))
                    );
                }


                targetPosition = transform.rotation * targetPosition;
                targetPosition += transform.position;

                GameObject inst = Instantiate(m_PrefabToSpawn, targetPosition, Quaternion.identity);
                inst.transform.localScale = size;
                inst.transform.parent = parent;

                grid[i].Add(inst);
            }
        }
    }
}

