﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Rewired;
using UnityEngine.SceneManagement;

public class Shooter : MonoBehaviour
{
    //public float projectileSpeed;
    //public AnimationCurve heightCurve;
    public float coolDown;
    float coolTime;
    public int onHitScoreLoose;
    //public int ammoammount;
    //public bool isHeat;
    //public GameObject projectile;
    //public GameObject launcher;
    public LayerMask target;
    //public ParticleSystem playerHitParticule;
    //public ParticleSystem wallHitParticule;

    private Player player;
    Scorer score;
    private GameObject ply;
    private MovingCursor mc;
    private ElevateBricks eb;
    float invT;
    float it;
    bool hasHit;
    // Start is called before the first frame update

    [Header("Events")]
    [SerializeField]
    private cursorEvents _onShoot = new cursorEvents();
    private cursorEvents _onHit = new cursorEvents();
    private cursorEvents _onMiss = new cursorEvents();
    void Start()
    {
        eb = FindObjectOfType<ElevateBricks>();
        ply = GameObject.FindGameObjectWithTag("Player");
        it = ply.GetComponent<CharacterControllerP1>().invulnerabilityTime;
        mc = FindObjectOfType<MovingCursor>();
        score = GameObject.FindObjectOfType<Scorer>();
        player = ReInput.players.GetPlayer("Player2");
    }

    // Update is called once per frame
    void Update()
    {
       
        if (player.GetButtonDown("Shoot")) Shoot();
        else if(player.GetButton("Shoot") && coolTime == 0) Shoot();
        if (coolTime > 0) StartCoolDown();

        if(hasHit)
        {

            if (invT < it)
            {
                invT += Time.deltaTime;
            }
            else
            {
                hasHit = false;
                invT = 0;
            }
        }
    }

    void StartCoolDown()
    {
        if (coolTime > 0)
        {
            coolTime -= Time.deltaTime;
        }
        if (coolTime <= 0) coolTime = 0;
    }

    void Shoot()
    {
        mc.Shoot();
        Debug.DrawLine(transform.position, transform.position + transform.forward * 10f, Color.red);
        _onShoot.Invoke(CursorInfo.Empty);
        Collider[] go = Physics.OverlapSphere(transform.position + transform.forward * 0.5f, 0.5f, target);
        Debug.Log("shoot");
        if (go.Length > 0)
        {
            _onHit.Invoke(CursorInfo.Empty);
            if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Menu"))
            {
                GameObject.FindObjectOfType<PlayButton>().SetHasTouched(true);
            }
            else
            {
                //Debug.Log(hasHit);
                if (!hasHit)
                {
                    Debug.Log("PlayerHit");
                    score.SubstractScore(ply.transform.position, onHitScoreLoose);
                    transform.GetComponent<AimController>().ToggleSlowed();
                    hasHit = true;
                }


            }
        }
        else
        {
            _onMiss.Invoke(CursorInfo.Empty);
            Debug.Log("WallHit");
            eb.AddWave(transform.position);
        }

        //GameObject go = Instantiate(projectile) as GameObject;
        //go.transform.position = launcher.transform.position;
        //go.AddComponent<Projectile>();
        //var dir = this.transform.position - launcher.transform.position;
        //var dist = Vector3.Distance(transform.position,new Vector3(transform.position.x, transform.position.y, 0));
        //go.GetComponent<Projectile>().SetValue(dir , projectileSpeed, dist, transform.position, heightCurve );
        //go.transform.parent = launcher.transform;
        coolTime = coolDown;
        //Debug.DrawLine(launcher.transform.position, transform.position, Color.cyan);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(Color.red.r, Color.red.g, Color.red.b, 0.5f);
        Gizmos.DrawWireSphere(transform.position + transform.forward * 0.5f, 0.5f );
    }
}
