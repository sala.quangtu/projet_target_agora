﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class ChangementJoystick : MonoBehaviour
{
    public Player runner = null;
    public Player hunter = null;

    public Joystick joy1 = null;
    public Joystick joy2 = null;

    private void Awake()
    {
        runner = ReInput.players.GetPlayer("Player1");
        hunter = ReInput.players.GetPlayer("Player2");

        if(ReInput.controllers.Joysticks.Count > 0)
        {
            joy1 = ReInput.controllers.Joysticks[0];

            if (ReInput.controllers.joystickCount > 1)
            {
                joy2 = ReInput.controllers.Joysticks[1];
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        SwitchJoy();
    }

    public void SwitchJoy()
    {
        if (ReInput.controllers.joystickCount > 1)
        {
            if(runner.controllers.ContainsController(joy1))
            {
                runner.controllers.RemoveController(joy1);
                hunter.controllers.AddController(joy1, true);

                hunter.controllers.RemoveController(joy2);
                runner.controllers.AddController(joy2, true);
            }
            else
            {
                runner.controllers.RemoveController(joy2);
                hunter.controllers.AddController(joy2, true);

                hunter.controllers.RemoveController(joy1);
                runner.controllers.AddController(joy1, true);
            }
        }
        else if (ReInput.controllers.joystickCount > 0)
        {
            if (runner.controllers.joystickCount > 0)
            {
                runner.controllers.RemoveController(joy1);
                hunter.controllers.AddController(joy1, true);
            }
            else if (hunter.controllers.joystickCount > 0)
            {
                hunter.controllers.RemoveController(joy1);
                runner.controllers.AddController(joy1, true);
            }
        }
    }
}