﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class SoundsGestioner : MonoBehaviour
{
    [SerializeField]
    public SO_SoundsList m_soundList = null;

    public static SoundsGestioner instance = null;

    private void Awake()
    {
        #region DontDestroyOnLoad
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        #endregion

        if (m_soundList.sounds.Length != 0)
        {
            for (int i = 0; i < m_soundList.sounds.Length; i++)
            {
                m_soundList.sounds[i].source = gameObject.AddComponent<AudioSource>();

                if (m_soundList.sounds[i].clip.Length > 1)
                {
                    int j = UnityEngine.Random.Range(0, m_soundList.sounds[i].clip.Length);
                    m_soundList.sounds[i].source.clip = m_soundList.sounds[i].clip[j];
                }
                else
                {
                    m_soundList.sounds[i].source.clip = m_soundList.sounds[i].clip[0];
                }

                m_soundList.sounds[i].source.volume = m_soundList.sounds[i].volume;
                m_soundList.sounds[i].source.pitch = m_soundList.sounds[i].pitch;
                m_soundList.sounds[i].source.loop = m_soundList.sounds[i].loop;
                m_soundList.sounds[i].source.playOnAwake = m_soundList.sounds[i].playOnAwake;
                m_soundList.sounds[i].source.outputAudioMixerGroup = m_soundList.sounds[i].output;
            }
        }
    }

    private void Update()
    {
        m_soundList.currentDuration -= Time.deltaTime;
    }
}