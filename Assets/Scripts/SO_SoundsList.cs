﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SO_SoundsList", menuName = "ScriptableObject/SO_SoundsList", order = 150)]
public class SO_SoundsList : ScriptableObject
{
    public float durationBetweenWalk = 0.5f;
    public float currentDuration = 0f;

    public Sound[] sounds = new Sound[0];

    public void PlaySound(string _name)
    {
        Sound s = null;

        for(int i = 0; i < sounds.Length; i ++)
        {
            if(sounds[i].name == _name)
            {
                s = sounds[i];
                break;
            }
        }

        if(s.tag == "Walk")
        {
            if(currentDuration <= 0f)
            {
                currentDuration = durationBetweenWalk;

                s.source.Stop();
                s.source.time = 0;

                if (s.clip.Length > 1)
                {
                    int j = Random.Range(0, s.clip.Length);
                    s.source.clip = s.clip[j];
                }

                s.source.Play();
            }
        }
        else if (s != null)
        {
            s.source.Stop();
            s.source.time = 0;

            if (s.clip.Length > 1)
            {
                int j = Random.Range(0, s.clip.Length);
                s.source.clip = s.clip[j];
            }

            s.source.Play();
        }
    }
}