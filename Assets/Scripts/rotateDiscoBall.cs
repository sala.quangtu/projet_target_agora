﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateDiscoBall : MonoBehaviour
{
    // Start is called before the first frame update
    public float rotationSpeed;
    public float rotationAngle;
    float rot;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        rot += rotationAngle * (rotationSpeed * Time.deltaTime);
        transform.rotation = Quaternion.Euler(-90, rot, transform.rotation.z);
    }
}
