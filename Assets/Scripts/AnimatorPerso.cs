﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorPerso : MonoBehaviour
{
    [SerializeField]
    private Animator m_animator = null;

    [SerializeField]
    private int m_idle = 0;

    private CharacterControllerP1 m_player = null;

    private void Start()
    {
        m_player = GetComponent<CharacterControllerP1>();
    }

    public void OnWalk()
    {
        m_animator.SetBool("IsWalking", true);
    }

    public void OnStopWalk()
    {
        m_animator.SetBool("IsWalking", false);
        m_idle = Random.Range(0, 2);
        m_animator.SetFloat("Idle", m_idle);

        if(m_player.GetLookDir())
        {
            m_animator.SetFloat("LookAt", 0f);
        }
        else
        {
            m_animator.SetFloat("LookAt", 1f);
        }
    }

    public void IsFear()
    {
        m_animator.SetFloat("WalkFear", 1f);
    }

    public void IsNotFear()
    {
        m_animator.SetFloat("WalkFear", 0f);
    }

    public void OnJump()
    {
        m_animator.SetBool("IsJumping", true);
        m_animator.SetBool("IsFalling", false);
    }

    public void OnFalling()
    {
        m_animator.SetBool("IsJumping", false);
        m_animator.SetBool("IsFalling", true);
    }

    public void OnHit()
    {
        m_animator.SetTrigger("IsHit");
    }

    public void OnLand()
    {
        m_animator.SetTrigger("IsGrounded");
        m_animator.SetBool("IsJumping", false);
        m_animator.SetBool("IsFalling", false);
    }
}