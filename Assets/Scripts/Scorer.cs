﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorer : MonoBehaviour
{
    [SerializeField]
    private int m_score = 0;
    public int Score { get { return m_score; } }

    [SerializeField]
    private int m_hit = 0;
    public int Hit { get { return m_hit; } }
    public void GotHit()
    {
        m_hit++;
    }

    public GameObject collectible;

    InfoInGame m_infoGame;

    private Vector3[] collectiblePos;
    public void AddScore(int _addScore)
    {
        m_score += _addScore;
        m_infoGame.UpdateScore();
    }

    public void SubstractScore(Vector3 playerPos, int _subScore)
    {
        int wantedsub = _subScore;


        if (m_score < _subScore)
        {
            wantedsub = m_score;

        }
        collectiblePos = new Vector3[Mathf.RoundToInt(Random.Range(1, wantedsub / 100))];
        for (int i = 0; i < collectiblePos.Length; i++)
        {
            collectiblePos[i] = Random.insideUnitSphere * 2.5f + (playerPos + Vector3.up * 2f);
            collectiblePos[i].z = -3;
            //while (Vector3.Distance(playerPos, collectiblePos[i]) < 1f)
            //{
            //    collectiblePos[i] = Random.insideUnitSphere * 2.5f + (playerPos + Vector3.up * 2f);
            //    collectiblePos[i].z = -3;
            //}
            //Debug.Log(collectiblePos[i]);
        }
        if (m_score > 0)
        {

            m_score -= wantedsub;


            SpawnLostCollectible(playerPos, collectiblePos);


        }

        GotHit();

        m_infoGame.UpdateOnlyScore();
        m_infoGame.UpdateHit();
    }

    public void SpawnLostCollectible(Vector3 ppos, Vector3[] pos)
    {
        for (int i = 0; i < pos.Length; i++)
        {
            var go = Instantiate(collectible) as GameObject;
            //Debug.Log("PlayerHit" + go);
            go.GetComponent<Collectible>().ToggleLost();
            go.transform.position = ppos + Vector3.up * 2f;
            go.GetComponent<Rigidbody>().AddForce((pos[i] - ppos) * 2.5f, ForceMode.Impulse);

        }
    }

    public void ResetScore()
    {
        m_score = 0;
        m_hit = 0;
    }

    private void Awake()
    {
        ResetScore();
        m_infoGame = FindObjectOfType<InfoInGame>();
    }

    private void OnDrawGizmos()
    {

        //for (int i = 0; i < collectiblePos.Length; i++)
        //{
        //    Gizmos.DrawSphere(collectiblePos[i], 0.3f);
        //}
    }
}