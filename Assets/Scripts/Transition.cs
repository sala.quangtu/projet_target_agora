﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Transition : MonoBehaviour
{
    [SerializeField]
    Animator m_transition = null;

    [SerializeField]
    float m_transitionTime = 1f;
    public float TransitionTime => m_transitionTime;

    private AudioSource m_audioSource = null;
    [SerializeField]
    private AudioClip m_sound_rideaux_open = null;
    [SerializeField]
    private AudioClip m_sound_rideaux_close = null;

    private void Awake()
    {
        m_audioSource = GetComponent<AudioSource>();

        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("SC_DEBUT"))
        {
            SceneManager.LoadScene("Sc_Menu2");
        }
        else
        {
            try
            {
                GameMaster gm = FindObjectOfType<GameMaster>();

                if (gm == null)
                {
                    SceneManager.LoadScene("SC_DEBUT");
                    return;
                }
            }
            catch (System.Exception)
            {
                throw;
            }

            FindObjectOfType<AudioManager>().NewScene();
        }
    }

    private void Start()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Menu2"))
        {

        }
        else if (SceneManager.GetActiveScene() != SceneManager.GetSceneByName("Sc_Transition") && SceneManager.GetActiveScene() != SceneManager.GetSceneByName("Sc_Fin"))
        {
            m_transition.SetTrigger("Start");
        }
        else if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Transition"))
        {
            GameMaster.GM.BetweenLevels();
        }
    }

    public void OpenUp()
    {
        m_transition.SetTrigger("Start");
    }

    public void SoundOpen()
    {
        m_audioSource.volume = 1;
        m_audioSource.time = 0;
        m_audioSource.loop = false;
        m_audioSource.clip = m_sound_rideaux_open;
        m_audioSource.Play();
    }

    public void SoundClose()
    {
        m_audioSource.volume = 1;
        m_audioSource.time = 0;
        m_audioSource.loop = false;
        m_audioSource.clip = m_sound_rideaux_close;
        m_audioSource.Play();
    }

    public void StartTheGame()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Ice")
            || SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Automne")
            || SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Neon")
            || SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Vegetation"))
        {
            GameMaster.GM.LaunchRound();
            FindObjectOfType<CollectiblesGestionner>().SetShowTime(true);
            FindObjectOfType<InfoInGame>().SetShowTime(true);
            FindObjectOfType<CharacterControllerP1>().ToggleActivate();
            FindObjectOfType<AimController>().ToggleActivate();
        }
        else if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Sc_Menu2"))
        {
            FindObjectOfType<CharacterControllerP1>().ToggleActivate();
            FindObjectOfType<AimController>().ToggleActivate();
        }
    }

    public void DropTheVolume()
    {
        FindObjectOfType<AudioManager>().DropTheVolume(m_transitionTime);
    }

    public void FirstLevelScene()
    {
        //Debug.Log("1ere scene");
        StartCoroutine(LoadFirstLevelScene());
    }

    public void SecondLevelScene(int p)
    {
        //Debug.Log("2eme scene");
        StartCoroutine(LoadSecondLevelScene(p));
    }

    public void TransitionScene()
    {
        //Debug.Log("transition scene");
        StartCoroutine(LoadTransitionScene());
    }

    public void EndScene()
    {
        //Debug.Log("End scene");
        StartCoroutine(LoadEndScene());
    }

    public void MenuScene()
    {
        //Debug.Log("Menu scene");
        StartCoroutine(LoadMenuScene());
    }

    private IEnumerator LoadFirstLevelScene()
    {
        m_transition.SetTrigger("Transitionning");

        yield return new WaitForSeconds(m_transitionTime);

        int i = 3;

        if (GameMaster.GM.ONE_SCENE)
        {
            i = 3;
        }
        else if(GameMaster.GM.TWO_SCENE)
        {
            i = Random.Range(3, 5);
        }
        else if(GameMaster.GM.THREE_SCENE)
        {
            i = Random.Range(3, 6);
        }
        else
        {
            i = Random.Range(3, 7);
        }
        
        GameMaster.GM.FirstScene = i;
        SceneManager.LoadScene(i);
    }

    private IEnumerator LoadSecondLevelScene(int p)
    {
        //Debug.Log("je passe par ici");
        //m_transition.SetTrigger("Transitionning");

        //yield return new WaitForSeconds(m_transitionTime);

        int i = 3;

        if (GameMaster.GM.ONE_SCENE)
        {
            i = 3;
        }
        else if (GameMaster.GM.TWO_SCENE)
        {
            if (p == 3)
            {
                i = 4;
            }
            else
            {
                i = 3;
            }
        }
        else if (GameMaster.GM.THREE_SCENE)
        {
            do
            {
                i = Random.Range(3, 6);
            } while (i == p);
        }
        else
        {
            do
            {
                i = Random.Range(2, 7);
            } while (i == p);
        }

        yield return null;

        SceneManager.LoadScene(i);
    }

    private IEnumerator LoadTransitionScene()
    {
        m_transition.SetTrigger("Transitionning");

        yield return new WaitForSeconds(m_transitionTime);

        SceneManager.LoadScene("Sc_Transition");
    }

    private IEnumerator LoadEndScene()
    {
        m_transition.SetTrigger("Transitionning");

        yield return new WaitForSeconds(m_transitionTime);

        SceneManager.LoadScene("Sc_Fin");
    }

    private IEnumerator LoadMenuScene()
    {
        m_transition.SetTrigger("Transitionning");

        yield return new WaitForSeconds(m_transitionTime);

        SceneManager.LoadScene("Sc_Menu2");
    }
}