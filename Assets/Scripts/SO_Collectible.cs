﻿using UnityEngine;

[CreateAssetMenu(fileName = "Collectible", menuName = "OtherSettings/Collectible", order = 150)]
public class SO_Collectible : ScriptableObject
{
    public int Score;
    public float RotationSpeed;
    public float MaxDistance;
    public float Distance;

    public void ActuScore(string _newValue)
    {
        if (int.TryParse(_newValue, out int result))
        {
            Score = result;
        }
    }

    public void ActuRotationSpeed(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            RotationSpeed = result;
        }
    }

    public void ActuMaxDistance(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            MaxDistance = result;
        }
    }

    public void ActuDistance(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            Distance = result;
        }
    }
}
