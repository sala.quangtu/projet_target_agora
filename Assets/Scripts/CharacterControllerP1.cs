﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;
public class CharacterControllerP1 : MonoBehaviour
{
    public SO_RunnerInfo Info;
    public float moveSpeed;
    //public float airSpeed;
    float speed;
    public float jumpHeight;
    public float gravity;

    public float reactivity;
    public float finalReact;
    public float accelTime;
    float accel;
    //public float deccelTime;
    public float bounceForce = 5f;
    //float deccel;

    Vector3 dir;
    Vector3 finalDir;
    Vector3 velocity;
    float x;
    float y;
    float checkTime = 0.015f;
    float time;
    bool grounded;
    bool collided;
    bool jump;
    bool propulse;
    bool bounce;
    Vector3 propulseDir;
    Vector3 wallNormal;
    public float propulseForce;
    float timeJump;

    Shooter enemi;
    bool isScared;

    public LayerMask collisionLayer;
    public LayerMask groundLayer;
    public LayerMask bounceLayer;

    public float groundDist;
    public float wallDist;

    public float invulnerabilityTime;
    public bool isInvulnerable;
    bool triggerInvEv;
    //int nbwallJump;
    int nbJump;
    Vector3 bounceHit;
    Vector3 bounceDir;
    Vector3 lastFinalDir;

    private Player player;

    bool gravityStart;
    bool goThrought;

    float resetAccelT;

    public bool activate = false;
    bool lookRight;
    bool triggerWalk;
    [Header("Events")]
    [SerializeField]
    private RunnerEvents _onWalkStart = new RunnerEvents();
    [SerializeField]
    private RunnerEvents _onStartScared = new RunnerEvents();
    [SerializeField]
    private RunnerEvents _onEndScared = new RunnerEvents();
    [SerializeField]
    private RunnerEvents _onJump = new RunnerEvents();
    [SerializeField]
    private RunnerEvents _onMidAir = new RunnerEvents();
    [SerializeField]
    private RunnerEvents _onLand = new RunnerEvents();
    [SerializeField]
    private RunnerEvents _onWalkStop = new RunnerEvents();
    [SerializeField]
    private RunnerEvents _onBump = new RunnerEvents();
    [SerializeField]
    private RunnerEvents _onFall = new RunnerEvents();
    [SerializeField]
    private RunnerEvents _StartInvul = new RunnerEvents();
    [SerializeField]
    private RunnerEvents _EndInvul = new RunnerEvents();
    [SerializeField]
    private RunnerEvents _OnCollectCoin = new RunnerEvents();
    public void ToggleActivate()
    {
        activate = true;
    }


    private void Start()
    {
        activate = false;
        player = ReInput.players.GetPlayer("Player1");
        //ReInput.players.GetPlayer("Player1").;

        speed = moveSpeed;
        // Time.timeScale = 0.5f;

        enemi = FindObjectOfType<Shooter>();
    }
    public void Update()
    {
        if(isInvulnerable && !triggerInvEv)
        {
            _StartInvul.Invoke(new RunnerInfo());
            triggerInvEv = true;
        }
        else if(!isInvulnerable && triggerInvEv)
        {
            _EndInvul.Invoke(new RunnerInfo());
            triggerInvEv = false;
        }
        if (Vector2.Distance(enemi.transform.position, transform.position) < 10f && !isScared)
        {
            isScared = true;
            _onStartScared.Invoke(new RunnerInfo());
        }
        else if (Vector2.Distance(enemi.transform.position, transform.position) >= 10f && isScared)
        {
            isScared = false;
            _onEndScared.Invoke(new RunnerInfo());
        }

        moveSpeed = Info.moveSpeed;
        jumpHeight = Info.jumpHeight;
        gravity = Info.gravity;
        reactivity = Info.reactivity;
        accelTime = Info.accelTime;
        bounceForce = Info.bounceForce;
        //Debug.Log("Time : " + Time.deltaTime);
        if (activate)
        {


            if (grounded) finalReact = reactivity;
            else finalReact = reactivity * 5f;
            CheckGround();
            CheckCapsuleDirCollision();
            ApplyGravity();
            velocity.y = Mathf.Clamp(velocity.y, -18, 20);
            var dir = GetInput();
            //GetSpeed();

            if (!collided) Move(dir);

            if (player.GetButtonDown("Jump") && jump)
            {
                _onJump.Invoke(RunnerInfo.Empty);
                Jump();
                //if (collided) nbwallJump++;
                nbJump++;
            }
            if (propulse) PropulsePlayer(propulseDir);
            //ReflectDir();
        }

    }
    Vector3 GetInput()
    {
        //var xtmp = x;
        x = player.GetAxisRaw("MoveHorizontalP1");
        y = player.GetAxisRaw("MoveVerticalP1");
        if (y < -0.9f) goThrought = true;
        else goThrought = false;
        //else goThrought = false;
        if (x < 0.25f && x > -0.25f)
        {
            x = 0;
            //        _onWalkStop.Invoke(RunnerInfo.Empty);
        }
        
        else
        {
            if (x < 0) lookRight = false;
            else lookRight = true;

            transform.GetChild(transform.childCount - 1).transform.rotation = Quaternion.LookRotation(Vector3.right * -x);
            transform.GetChild(transform.childCount - 2).transform.rotation = Quaternion.LookRotation(Vector3.right * -x);
        }
        //Debug.Log(lookRight);
        //if (x != xtmp)
        //{
        //    startTime = true;
        //    time = 0;
        //}
        dir = (transform.right * x).normalized;

        if (!bounce) finalDir = Vector3.Lerp(finalDir, dir, finalReact * Time.deltaTime);
        else finalDir = Vector3.Lerp(finalDir, dir, reactivity * Time.deltaTime);

        //Debug.DrawLine(transform.position, transform.position + new Vector3(previousFinalDir.x * 2, velocity.y, 0).normalized * 1.5f, Color.green);


        //else isDash = false;

        return finalDir;
    }
    //void GetSpeed()
    //{

    //    if (x == 0)
    //    {
    //        //float rate = 1.0f / deccelTime;
    //        //if (deccel > 0)
    //        //{
    //        //    deccel -= Time.deltaTime * rate;
    //        //}
    //        speed = Mathf.Lerp(speed, 0, deccelTime * Time.deltaTime);
    //        if (speed < 0.05f) speed = 0;
    //    }
    //    else
    //    {

    //        speed = moveSpeed;
    //    }
    //}
    void ApplyGravity()
    {
        if (!grounded)
        {
            velocity.y += gravity * Time.deltaTime;
            transform.position += velocity * Time.deltaTime;

        }
        else velocity = Vector3.zero;
        if (velocity.y < 0 && !gravityStart)
        {
            gravityStart = true;
            _onMidAir.Invoke(new RunnerInfo());
        }
        else gravityStart = false;

        if (gravityStart)
        {
            _onFall.Invoke(RunnerInfo.Empty);
        }
        //Debug.Log("ApplyGravity : " + gravityStart);
    }
    void Move(Vector3 dir)
    {
        if (x != 0 || bounce)
        {
            float rate = 1.0f / accelTime;
            if (accel < 1)
            {
                accel += Time.deltaTime * rate;
            }
        }
        else
        {

            if (resetAccelT < 0.2f)
            {
                resetAccelT += Time.deltaTime;
            }
            else
            {
                accel = 0;
                resetAccelT = 0;
            }
            /* Mathf.Lerp(accel, 0, deccelTime * Time.deltaTime)*/
        }
        if (x != 0 && grounded && !triggerWalk)
        {
            _onWalkStart.Invoke(RunnerInfo.Empty);
            triggerWalk = true;
        }
        else if (x == 0 && grounded && triggerWalk)
        {
            triggerWalk = false;
            _onWalkStop.Invoke(RunnerInfo.Empty);
        }

        transform.position += dir * (speed * accel) * Time.deltaTime;
        var yb = Mathf.Clamp(transform.position.y, -4.3f, 3f);
        var xb = Mathf.Clamp(transform.position.x, -9.15f, 9.15f);
        //CheckCapsuleDirCollision();
        transform.position = new Vector3(xb, yb, transform.position.z);
    }
    void CheckCapsuleDirCollision()
    {
        var sPosTop = transform.position + new Vector3(-x, -0.25f, 0);
        var sPosBot = transform.position + new Vector3(-x, 0.25f, 0);
        //var dir = new Vector3(finalDir.x, velocity.y, 0);
        if (Physics.CapsuleCast(sPosTop, sPosBot, 0.5f, finalDir.normalized, out RaycastHit hit, wallDist + Mathf.Abs(x), collisionLayer))
        {
            //if (nbwallJump < 5)
            //{
            //    jump = true;
            //}
            wallNormal = hit.normal;
            /*if (walkable) */
            collided = true;
            //Debug.Log("Colllllllide");
            //else collided = false;
            if (Vector3.Distance(transform.position, new Vector3(hit.point.x, transform.position.y, hit.point.z)) < wallDist)
            {
                transform.position = Vector3.Lerp(transform.position, transform.position + hit.normal * wallDist, 15f * Time.deltaTime);
            }
            //wallTempDir = Vector3.Cross(new Vector3(Mathf.Abs(hit.normal.x), Mathf.Abs(hit.normal.y), Mathf.Abs(hit.normal.z)), Vector3.up);
            //Debug.DrawLine(transform.position, transform.position + temp2 * 3f, Color.yellow);
            return;

        }

        else collided = false;
    }
    void CheckGround()
    {
        //if (!Physics.CheckSphere(transform.position - new Vector3(0, 0.25f, 0), 0.5f, groundLayer))
        //{
        //    //Debug.Log("Not Grounded");
        //    grounded = false;
        //    jump = false;

        //}
        if (!Physics.CheckBox(transform.position - new Vector3(0, groundDist - 0.125f, 0), new Vector3(0.25f, 0.025f, 0.25f), Quaternion.identity, groundLayer))
        {
            grounded = false;
            if (nbJump > 0) jump = false;
        }
        else
        {

            ////if (nbwallJump > 0) nbwallJump = 0;

            if (gravityStart)
            {
                //Ray ray = new Ray(transform.position + new Vector3(0.5f, 0, 0), Vector3.down);
                //Ray ray2 = new Ray(transform.position + new Vector3(-0.5f, 0, 0), Vector3.down);

                //RaycastHit hit;
                //RaycastHit hit2;


                //if (Physics.Raycast(ray, out hit, groundDist + 0.15f, groundLayer))
                //{

                //    //Debug.Log("lol");
                //    if (hit.collider.tag != "Platform")
                //    {
                //    }

                //    if (!goThrought)
                //    {
                //        if (Vector3.Distance(transform.position, hit.point) < groundDist)
                //        {
                //            transform.position = Vector3.Lerp(transform.position, transform.position + hit.normal * groundDist, 25f * Time.deltaTime);
                //            //Debug.Log("Replace Player");
                //        }
                //    }
                //    else
                //    {
                //        if (hit.collider.tag != "Platform")
                //        {
                //            if (Vector3.Distance(transform.position, hit.point) < groundDist)
                //            {
                //                transform.position = Vector3.Lerp(transform.position, transform.position + hit.normal * groundDist, 25f * Time.deltaTime);
                //                //Debug.Log("Replace Player");
                //            }
                //        }
                //    }



                //}
                //else if (Physics.Raycast(ray2, out hit2, groundDist + 0.15f, groundLayer))
                //{
                //    if (!goThrought)
                //    {
                //        if (Vector3.Distance(transform.position, hit2.point) < groundDist)
                //        {
                //            transform.position = Vector3.Lerp(transform.position, transform.position + hit2.normal * groundDist, 25f * Time.deltaTime);
                //            Debug.Log("Replace Player");
                //        }
                //    }
                //    else
                //    {
                //        if (hit2.collider.tag != "Platform")
                //        {
                //            if (Vector3.Distance(transform.position, hit2.point) < groundDist)
                //            {
                //                transform.position = Vector3.Lerp(transform.position, transform.position + hit2.normal * groundDist, 25f * Time.deltaTime);
                //                Debug.Log("Replace Player");
                //            }
                //        }
                //    }
                //}

            }

            if (velocity.y <= 0)
            {
                Ray rayG = new Ray(transform.position, Vector3.down);
                if (Physics.SphereCast(rayG, 0.5f, out RaycastHit hitg, groundDist, groundLayer))
                {
                    if (hitg.collider.tag == "Platform" && goThrought)
                    {
                        grounded = false;
                    }
                    if (velocity.y < 0)
                    {
                        if (hitg.collider.tag == "Platform" && !goThrought)
                        {
                            grounded = true;
                            _onLand.Invoke(RunnerInfo.Empty);
                        }
                        else if (hitg.collider.tag != "Platform")
                        {
                            if (!grounded)
                            {
                                Vector3 hitPos = new Vector3(hitg.point.x, hitg.point.y, 0);
                                Vector3 plyPos = new Vector3(hitg.point.x, transform.position.y, 0);
                                float d = Vector3.Distance(hitPos, plyPos);
                                if (d < groundDist)
                                {
                                    transform.position = Vector3.Lerp(transform.position, transform.position + hitg.normal * (groundDist - d), 50f * Time.deltaTime);

                                    //Debug.Log("Replace Player");
                                }
                                else
                                {
                                    grounded = true;
                                    _onLand.Invoke(RunnerInfo.Empty);
                                }
     
                            }



                        }
                    }
                }
                if(grounded) jump = true;
            }
            //else
            //{
            //  grounded = true;

            //}
            if (finalDir != transform.right * x) finalDir = transform.right * x;
            propulse = false;
            bounce = false;
            nbJump = 0;

            //Debug.Log("6 pied sous terre enculé");

        }




        //if (goThrought)
        //{
        //    if (grounded)
        //    {
        //        goThrought = false;
        //    }
        //    else
        //    {
        //        if (time < checkTime)
        //        {
        //            time += Time.deltaTime;
        //        }
        //        else
        //        {
        //            goThrought = false;
        //            time = 0;
        //        }
        //    }
        //}
    }
    void Jump()
    {
        timeJump += Time.deltaTime;
        velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        transform.position += velocity * Time.deltaTime;
        if (collided)
        {
            propulse = true;
            propulseDir = wallNormal + Vector3.up * 0.5f;
            Debug.DrawLine(transform.position, transform.position + finalDir, Color.blue);
        }
        if (timeJump >= 0.1f)
        {

            jump = false;
            timeJump = 0;
        }

    }

    void PropulsePlayer(Vector3 dir)
    {
        Debug.DrawLine(transform.position, transform.position + dir * 3f, Color.blue);

        finalDir = dir;
        Jump();
        //Move(finalDir);
        //velocity.y = 0;
        propulse = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("in trigger");
        if (other.tag == "Bumper")
        {
            _onBump.Invoke(RunnerInfo.Empty);
            // Debug.Log("VER LINFINI ET LODELA");
            //lastFinalDir.x = -lastFinalDir.x;
            propulse = true;
            bounce = true;
            //bounceHit = hit.point;
            var norm = (other.transform.position - transform.position);
            var hit = (transform.position - other.transform.position);
            bounceDir = Vector3.Reflect(transform.position + new Vector3(lastFinalDir.x, velocity.y, lastFinalDir.z), norm.normalized).normalized * bounceForce;
            //bounceDir = Vector3.Cross(refD, Vector3.up);
            var reflectedDir = hit + bounceDir;
            reflectedDir.x = -(reflectedDir.x * propulseForce * 2);
            //finalDir = reflectedDir;
            reflectedDir.z = 0;
            PropulsePlayer(reflectedDir.normalized * propulseForce);
            other.GetComponent<ScaleUp>().Bumped();
            //hit.collider.GetComponent<bumperAnim>().Bumped();
            //Debug.Log("BOUNCE MODAFUKA");
        }
    }
    private void OnTriggerStay(Collider other)
    {
        //Debug.Log("in trigger");
        if (other.tag == "Bumper")
        {
            //Debug.Log("VER LINFINI ET LODELA");
            //lastFinalDir.x = -lastFinalDir.x;
            propulse = true;
            bounce = true;
            //bounceHit = hit.point;
            var norm = (other.transform.position - transform.position);
            var hit = (transform.position - other.transform.position);
            bounceDir = Vector3.Reflect(transform.position + new Vector3(lastFinalDir.x, velocity.y, lastFinalDir.z), norm.normalized).normalized * bounceForce;
            //bounceDir = Vector3.Cross(refD, Vector3.up);
            var reflectedDir = hit + bounceDir;
            //finalDir = reflectedDir;
            reflectedDir.z = 0;
            reflectedDir.x = -(reflectedDir.x * propulseForce * 2);
            PropulsePlayer(reflectedDir.normalized * propulseForce);
            other.GetComponent<ScaleUp>().Bumped();
            //hit.collider.GetComponent<bumperAnim>().Bumped();
            //Debug.Log("BOUNCE MODAFUKA");
        }
    }

    //void ReflectDir()
    //{
    //    //Vector3 reflectedDir = Vector3.zero;
    //    lastFinalDir.x = -lastFinalDir.x;
    //    Ray ray = new Ray(transform.position, new Vector3(finalDir.x, velocity.y, finalDir.z));
    //    RaycastHit hit;
    //    if (Physics.Raycast(ray, out hit, 0.5f, bounceLayer))
    //    {
    //        propulse = true;
    //        bounce = true;
    //        bounceHit = hit.point;
    //        bounceDir = Vector3.Reflect(transform.position + new Vector3(lastFinalDir.x, velocity.y, lastFinalDir.z), hit.normal).normalized * bounceForce;
    //        //bounceDir = Vector3.Cross(refD, Vector3.up);
    //        var reflectedDir = bounceHit + bounceDir;
    //        finalDir = reflectedDir;
    //        reflectedDir.z = 0;
    //        PropulsePlayer(reflectedDir);
    //        hit.collider.GetComponent<ScaleUp>().Bumped();
    //        //hit.collider.GetComponent<bumperAnim>().Bumped();
    //        //Debug.Log("BOUNCE MODAFUKA");
    //    }
    //    else
    //    {
    //        lastFinalDir = finalDir;
    //    }

    //}
    public void OnCollect()
    {
        Debug.Log("ICI !");
        _OnCollectCoin.Invoke(RunnerInfo.Empty);
    }
    public void TeleportPlayer(Vector3 position)
    {
        transform.position = new Vector3(position.x, position.y, transform.position.z);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        var wantedDir = new Vector3(finalDir.x * 2, velocity.y, 0);
        Gizmos.DrawLine(transform.position, transform.position + wantedDir.normalized * 3f);
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position + new Vector3(-x, -0.25f, 0), transform.position + new Vector3(0, -0.25f, 0) + wantedDir.normalized * wallDist);

        Gizmos.DrawLine(transform.position + new Vector3(-x, 0.25f, 0), transform.position + new Vector3(0, 0.25f, 0) + wantedDir.normalized * wallDist);

        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(bounceHit, bounceHit + bounceDir);
        //Gizmos.color = Color.green;
        //var wantedReflect = Vector3.Cross(bounceHit + (bounceDir - bounceHit), Vector3.right);
        //Gizmos.DrawLine(transform.position, bounceHit - wantedReflect);
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireCube(transform.position - new Vector3(0, 0.5f, 0), new Vector3(0.5f, 0.25f, 0.5f));
    }

    public bool GetLookDir()
    {
        return lookRight;
    }
}
