﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectiblesGestionner : MonoBehaviour
{
    public SO_Collectible SO_Collectible;

    [SerializeField, Tooltip("SPawn des collectibles")]
    private Collectible[] m_colletibles = new Collectible[12];

    [SerializeField]
    private List<Collectible> m_disponible = new List<Collectible>();
    [SerializeField]
    private List<Collectible> m_spawned = new List<Collectible>();

    [SerializeField, Tooltip("temps entre chaque spawn")]
    private float m_timer = 0f;

    private float m_wait = 0;

    [SerializeField]
    private bool _canSpawn;

    [SerializeField]
    private bool m_showTime = false;
    public bool showTime => m_showTime;
    public void SetShowTime(bool _value)
    {
        m_showTime = _value;
    }

    //private IEnumerator coroutine;

    private void Awake()
    {
        //coroutine = SpawnTime();

        if(m_disponible.Count > 0)
        {
            m_disponible.Clear();
        }

        if(m_spawned.Count > 0)
        {
            m_spawned.Clear();
        }


        for (int i = 0; i < m_colletibles.Length; i++)
        {
            m_disponible.Add(m_colletibles[i]);
            m_disponible[i].gameObject.SetActive(false);
        }

        m_disponible.Shuffle();
    }

    private void Start()
    {
        _canSpawn = false;
        m_showTime = false;
    }

    private void Update()
    {
        if(!showTime)
        {
            return;
        }

        m_wait += Time.deltaTime;

        if(m_wait >= m_timer && m_disponible.Count > 0)
        {
            m_wait = 0f;
            _canSpawn = true;
        }

        if(_canSpawn)
        {
            SpawnCollectible();
        }
    }

    public void SpawnCollectible()
    {
        if (m_disponible.Count > 0)
        {
            Collectible _collectible = m_disponible[0];
            m_spawned.Add(_collectible);
            m_disponible.Remove(_collectible);
            _collectible.gameObject.SetActive(true);
            _collectible.ResetState();
            _collectible.Spawn();
            _canSpawn = false;
        }
    }

    //public IEnumerator SpawnTime()
    //{
    //    Debug.Log("j'attend pour spawn");
    //    yield return new WaitForSeconds(m_timer);
    //    Debug.Log("j'ai fini d'attendre pour spawn");
    //    _canSpawn = true;
    //}

    public void CollectibleCollected(Collectible _collected)
    {
        m_spawned.Remove(_collected);
        m_disponible.Add(_collected);
        _collected.gameObject.SetActive(false);
    }
}