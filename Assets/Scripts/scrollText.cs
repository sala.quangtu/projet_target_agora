﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scrollText : MonoBehaviour
{
    // Start is called before the first frame update
    MeshRenderer mr;
    Vector2 move;
    public float speed = 0.2f;
    void Start()
    {
        mr = transform.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        move.x += speed * Time.deltaTime;
        mr.material.SetTextureOffset("_BaseColorMap",move);
        mr.material.SetTextureOffset("_EmissiveColorMap", move);
    }
}
