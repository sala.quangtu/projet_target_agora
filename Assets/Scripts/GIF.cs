﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class GIF : MonoBehaviour
{
    [SerializeField]
    private bool m_J1Pressed = false;
    [SerializeField]
    private GameObject m_bAJ1 = null;
    [SerializeField]
    private GameObject m_bAJ1Pressed = null;

    [Space, SerializeField]
    private bool m_J2Pressed = false;
    [SerializeField]
    private GameObject m_bAJ2 = null;
    [SerializeField]
    private GameObject m_bAJ2Pressed = null;

    private Player J1 = null;
    private Player J2 = null;
    private Animator m_animator = null;

    [SerializeField]
    private Animator m_racoonAnimator = null;

    [SerializeField]
    private GameObject m_coin = null;
    [SerializeField]
    private GameObject m_collectible = null;

    [SerializeField]
    private SO_SoundsList _soundList = null;

    private void Awake()
    {
        m_animator = GetComponent<Animator>();
        J1 = ReInput.players.GetPlayer("Player1");
        J2 = ReInput.players.GetPlayer("Player2");
        m_bAJ2.SetActive(true);
        m_bAJ2Pressed.SetActive(false);
        m_bAJ1.SetActive(true);
        m_bAJ1Pressed.SetActive(false);
        m_coin.SetActive(false);
        m_collectible.SetActive(false);
    }

    private void Update()
    {
        if(J2.GetButtonDown("Shoot"))
        {
            m_J2Pressed = true;
            m_bAJ2.SetActive(false);
            m_bAJ2Pressed.SetActive(true);
        }

        if(J1.GetButtonDown("Jump"))
        {
            m_J1Pressed = true;
            m_bAJ1.SetActive(false);
            m_bAJ1Pressed.SetActive(true);
        }

        if(m_J2Pressed && m_J1Pressed)
        {
            LaunchFinal();
        }
    }

    public void PlayCollectibleSound()
    {
        _soundList.PlaySound("Collectible Collect");
    }

    public void PlayShootSound()
    {
        _soundList.PlaySound("Hit");
    }

    public void PlayPortailSound()
    {
        _soundList.PlaySound("Portail Enter");
    }

    public void StartTheGame()
    {
        gameObject.SetActive(false);
        FindObjectOfType<Transition>().OpenUp();
    }

    public void IsFear()
    {
        m_racoonAnimator.SetTrigger("Fear");
    }

    public void IsUnear()
    {
        m_racoonAnimator.SetTrigger("Unfear");
    }

    public void Coin()
    {
        m_coin.SetActive(true);
        m_collectible.SetActive(false);
    }

    public void Collectible()
    {
        m_coin.SetActive(false);
        m_collectible.SetActive(true);
    }

    public void LaunchFinal()
    {
        m_animator.SetTrigger("Final");
    }
}