﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "SO_SoundGestionner", menuName = "ScriptableObject/SO_SoundGestionner", order = 150)]
public class SO_SoundGestionner : ScriptableObject
{
    public AudioMixer masterMixer = null;
    public float Master = 0f;
    public float Music = 0f;
    public float Rideaux = 0f;
    public float Coins = 0f;
    public float Hit = 0f;
    public float Deplacement = 0f;
    public float Portails = 0f;
    public float Game = 0f;

    public void ActuMaster(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            Master = result;

            masterMixer.SetFloat("Volume_Master", Master);
        }
    }

    public void ActuMusic(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            Music = result;

            masterMixer.SetFloat("Volume_Music", Music);
        }
    }

    public void ActuRideaux(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            Rideaux = result;

            masterMixer.SetFloat("Volume_Rideaux", Rideaux);
        }
    }

    public void ActuCoins(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            Coins = result;
        }

        masterMixer.SetFloat("Volume_Coin", Coins);
    }

    public void ActuHit(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            Hit = result;
        }

        masterMixer.SetFloat("Volume_Hit", Hit);
    }

    public void ActuDeplacement(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            Deplacement = result;
        }

        masterMixer.SetFloat("Volume_Deplacement", Deplacement);
    }

    public void ActuPortails(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            Portails = result;
        }

        masterMixer.SetFloat("Volume_Portails", Portails);
    }

    public void ActuGame(string _newValue)
    {
        if (float.TryParse(_newValue, out float result))
        {
            Game = result;
        }

        masterMixer.SetFloat("Volume_Game", Game);
    }
}